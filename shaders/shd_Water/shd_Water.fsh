varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float time;
uniform float strength;
uniform sampler2D displacement_map;

void main() {
	vec2 time_offset = vec2(time/strength, 0.0);
	// Get the displacement color of the pixel, but offset by time
	vec4 displacement_color = texture2D(displacement_map, v_vTexcoord + time_offset);
	// Create distortion on the base texture by the brightness of the displacement map
	float brightness_range = ((displacement_color.r + displacement_color.g + displacement_color.b)/3.0 - 0.5)/strength; // from -0.5 to 0.5 for easier offset of the (u,v) coords which range from 0 to 1

	vec2 brightness_offset = vec2(brightness_range, 0.0);
	gl_FragColor = v_vColour * texture2D(gm_BaseTexture, v_vTexcoord + brightness_offset);
}
