varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float time;
uniform float pixels_width;
uniform float pixels_height;

void main() {
	vec2 pixels = v_vTexcoord;
	
	pixels.y = pixels.y + sin(pixels.x + time);
	pixels.y = max(pixels.y, 0.0);
	
	gl_FragColor = v_vColour * texture2D(gm_BaseTexture, pixels);
}
