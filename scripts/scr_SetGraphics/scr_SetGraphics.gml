/// @function				fnc_SetGraphics(w,h)
/// @description			Configures the game to match the specified resolution width and height
/// @param {real} w			OPTIONAL The desired width. If left blank uses BASE_RESOLUTION_W macro.
/// @param {real} h			OPTIONAL The desired height. If left blank uses BASE_RESOLUTION_H macro.

function fnc_SetGraphics(_resolution_w = BASE_RESOLUTION_W, _resolution_h = BASE_RESOLUTION_H) {
	/*
		if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW) { // Adjust desired Game resolution (camera) to match display size/aspect ratio and then set window equal to game resolution
		
			// Adjust resolution with the selected aspect ratio
		
			if (SELECTED_ASPECT_RATIO<1) { // Portrait aspect ratio
				adjusted_resolution_width = min(_resolution_w, DISPLAY_WIDTH);
				adjusted_resolution_height = floor(adjusted_resolution_width / SELECTED_ASPECT_RATIO);
			}
			else { // Landscape aspect ratio
				adjusted_resolution_height = min(_resolution_h, DISPLAY_HEIGHT);
				adjusted_resolution_width = floor(adjusted_resolution_height * SELECTED_ASPECT_RATIO);
			}
		
			// Keep window size except fullscreen (in which case window size = display size)
		
			if (window_get_fullscreen()) {
				adjusted_window_width = DISPLAY_WIDTH;
				adjusted_window_height = DISPLAY_HEIGHT;			
			}
			else {			
				adjusted_window_width = BASE_WINDOW_SIZE_W;
				adjusted_window_height = BASE_WINDOW_SIZE_H;
			}
		}
		else if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW || SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) { // Leave Game resolution (camera) fixed (as long as <= window) and adjust desired window resolution to match display size/aspect ratio
		
			// Adjust window size with the selected aspect ratio
		
			if (SELECTED_ASPECT_RATIO < 1) { // Portrait aspect ratio
				adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
				adjusted_window_height = floor(adjusted_window_width / SELECTED_ASPECT_RATIO);
			}
			else { // Landscape aspect ratio
				adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
				adjusted_window_width = floor(adjusted_window_height * SELECTED_ASPECT_RATIO);
			}
	
			if (window_get_fullscreen()) {
				adjusted_window_width = DISPLAY_WIDTH;
				adjusted_window_height = DISPLAY_HEIGHT;			
			}
			else {			
				adjusted_window_width = BASE_WINDOW_SIZE_W;
				adjusted_window_height = BASE_WINDOW_SIZE_H;
			}
		
			// Clamp resolution size to the window size
			adjusted_resolution_width = BASE_RESOLUTION_W;
			adjusted_resolution_height = BASE_RESOLUTION_H;
			//adjusted_resolution_width = min(_resolution_w, adjusted_window_width);
			//adjusted_resolution_height = min(_resolution_h, adjusted_window_height);		
		
		}
	
		// Redefine camera size
		camera_set_view_pos(VIEW, 0, 0);
		camera_set_view_size(VIEW, adjusted_resolution_width, adjusted_resolution_height);
	
		// Resize the application surface to the desired game resolution width/height
		//surface_resize(application_surface, adjusted_resolution_width, adjusted_resolution_height);
		surface_resize(application_surface, min(adjusted_window_width, adjusted_resolution_width), min(adjusted_window_height, adjusted_resolution_height));
	
	
		// Set window size
		window_set_size(adjusted_window_width, adjusted_window_height);
	
		// Browser maximize hack	
		if (BROWSER && option_value[? "Fullscreen"]) {
			window_set_size(browser_height*SELECTED_ASPECT_RATIO, browser_height);
		}
	
		// Center the window (on the next few step)
		if (CENTER_WINDOW) {
			alarm[0] = 2;
		}
	*/
	adjusted_resolution_width = BASE_RESOLUTION_W;
	adjusted_resolution_height = BASE_RESOLUTION_H;
	adjusted_window_width = BASE_WINDOW_SIZE_W;
	adjusted_window_height = BASE_WINDOW_SIZE_H;
	camera_set_view_pos(VIEW, 0, 0);
	camera_set_view_size(VIEW, adjusted_resolution_width, adjusted_resolution_height);
	window_set_size(adjusted_window_width, adjusted_window_height);
	surface_resize(application_surface, adjusted_window_width, adjusted_window_height);
	if (CENTER_WINDOW) {
		alarm[0] = 2;
	}
}