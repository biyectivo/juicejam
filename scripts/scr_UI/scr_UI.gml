
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		if (ENABLE_LIVE && live_call()) return live_result;
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _y_title = 60;
		
		type_formatted(_w/2, _y_title, "[fnt_Title][fa_middle][fa_center][scale,0.5][c_purple]=[c_white]Escape from Io[c_purple]=");
		type_formatted(_w/2, _y_title+100, "[c_white][fnt_HUD][fa_middle][fa_center][scale,1]A juicy game by biyectivo");
		type_formatted(_w/2, _y_title+120, "[c_gray][fnt_HUD][fa_middle][fa_center][scale,0.9]Art by Maaot @ itch.io");
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 220;
		var _spacing = 60;
		for (var _i = 0; _i<_n; _i++) {	
			if (menu_enabled[_i]) {
				fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center][c_white][fnt_Menu][scale,0.5]"+menu_items[_i], "[fa_middle][fa_center][c_aqua][fnt_Menu][scale,0.5]"+menu_items[_i], fnc_ExecuteMenu, _i);
			}
			else {
				type_formatted(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center][c_gray][fnt_Menu][scale,0.5]"+menu_items[_i]);
			}
		}
		
		
			
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		/*
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}*/
	}


	function fnc_Menu_2 () {
		/*
		room_goto(room_UI_Options);
		*/
	}

	
	function fnc_Menu_3 () {
		started = true;
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;

		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = GUI_MOUSE_X;
				var _mousey = GUI_MOUSE_Y;
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 60;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-30, _link_color+"[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;

	draw_set_alpha(0.7);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	
	
	Game.camera_shake = false;
	audio_stop_all();
	
	
	if (Game.lost) {	
		type_formatted(_w/2, 70, "[fa_center][fa_middle][fnt_Menu][scale,0.6][c_white]You couldn't escape Io alive!");
		type_formatted(_w/2, 110, "[fa_center][fa_middle][fnt_Menu][scale,0.3][c_aqua]You managed to save  [scale,0.5]"+string(Game.total_score)+"/"+string(Game.total_possible_score)+"[scale,0.3]  magic flowers.");
	}	
	else if (Game.won) {
		type_formatted(_w/2, 70, "[fa_center][fa_middle][fnt_Menu][scale,0.7][c_white]You escaped Io!");		
		if (random(1) < 0.1) {
			repeat(25) {
				var _x = irandom_range(0, room_width);
				var _y = irandom_range(0, 80);
				part_particles_create(Game.particle_system, _x, _y, Game.part_congratulation_star, 1);
			}
		}
		part_system_drawit(Game.particle_system);
		part_system_update(Game.particle_system);
		
		type_formatted(_w/2, 110, "[fa_center][fa_middle][fnt_Menu][scale,0.3][c_aqua]You managed to save  [scale,0.5]"+string(Game.total_score)+"/"+string(Game.total_possible_score)+"[scale,0.3]  magic flowers.");
		type_formatted(_w/2, 180, "[fa_center][fa_middle][fnt_Menu][scale,0.3][c_white]You managed to escape in:  [scale,0.5]"+time_format(Game.total_time));
	}
	
	type_formatted(_w/2, 30, "[fa_center][fa_middle][fnt_Menu][scale,0.3][$666666]SEED: "+str(Game.seed));
	
	#region Scoreboard
		if (ENABLE_SCOREBOARD) {
			// Scoreboard
			if (!scoreboard_queried) {
				//http_call = "query_scoreboard";
				var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
				http_get_id_query = http_get(_scoreboard_url);
				scoreboard_queried = true;
				if (current_scoreboard_updates < max_scoreboard_updates) {				
					Game.alarm[2] = timer_scoreboard_updates;
					current_scoreboard_updates++;
				}
			}
			else {
				if (http_return_status_query == 200) {
				
							
					var _list = ds_map_find_value(scoreboard_html5, "default");
					var _n = ds_list_size(_list);
					for (var _i=0; _i<_n; _i++) {
						var _map = ds_list_find_value(_list, _i);
						//show_debug_message(string(_i)+": "+string(_map[? "username"])+" = "+string(_map[? "game_score"]));
						draw_set_color(c_white);
						
						if (_i==0) {
							var _total_high_score = _map[? "game_score"];
						}
						else if (_i==4) {
							var _number_5_score	= _map[? "game_score"]; 
						}
					
						type_formatted(80, 400+_i*50, "[fa_right][scale,0.6]#"+string(_i+1));
						type_formatted(120, 400+_i*50, "[fa_left][scale,0.6]"+_map[? "username"]);
						type_formatted(500, 400+_i*50, "[fa_right][scale,0.6]"+_map[? "game_score"]);
					}
				
				
				}
				else if (http_return_status_query == noone) {
					type_formatted(_w/2, 400, "[fa_center][fa_middle][scale,0.7]Loading scoreboard...");
				}
			
			}
		}
	#endregion

	type_formatted(_w/2, GUI_HEIGHT-40, "[fa_center][fa_middle][fnt_Menu][scale,0.3][c_white]Press (A) or ENTER to try again", true, true, 10);
}



function fnc_DrawHUD() {
	fnc_BackupDrawParams();
	// Draw the HUD
	
	//var _player_tile_col = floor(obj_Player.x/TILE_SIZE);
	var _player_tile_row = floor(obj_Player.y/TILE_SIZE);
	
	
	var _width = 150;
	var _height = 150;
	var _margin = 20;
	var _x1 = GUI_WIDTH-_width-_margin;
	var _x2 = GUI_WIDTH-_margin;
	var _y1 = _margin;
	var _y2 = _margin + _height;
	
	if (ENABLE_LIVE && live_call()) return live_result;
	draw_set_color($222222);
	draw_set_alpha(0.6);	
	draw_roundrect(_x1, _y1, _x2, _y2, false);
	draw_set_alpha(1);
	draw_set_color($666666);
	draw_roundrect(_x1, _y1, _x2, _y2, true);
	type_formatted(_x1+5, _y1+5, "[fa_left][fa_top][fnt_HUD][scale,0.6][$CCCCCC]MINIMAP");
	
	var _xPlayer = obj_Player.x / room_width;
	//var _y = obj_Player.y / room_height;
	var _yPlayer = (obj_Player.y - camera_get_view_y(VIEW)) / camera_get_view_height(VIEW);
	
	if (_yPlayer*_height >= _y1 && _yPlayer*_height <= _y2) {
		draw_circle_color(_x1+_xPlayer*_width, _y1+_yPlayer*_height, 3, c_purple, c_purple, false);
	}
	
	var _total = instance_number(obj_BlueFlower);
	for (var _i=0; _i<_total; _i++) {
		var _id = instance_find(obj_BlueFlower, _i);
		if (_id.sprite_index == spr_BlueFlower_Closed) {
			var _x = _id.x/room_width;
			var _y = (_id.y - camera_get_view_y(VIEW))/ camera_get_view_height(VIEW);
			if (_y*_height >= _y1 && _y*_height <= _y2) {
				draw_circle_color(_x1+_x*_width, _y1+_y*_height, 2, c_aqua, c_aqua, false);
			}
		}		
	}
	
	var _ylava = -1;
	if (instance_exists(obj_LavaArea)) {
		var _ylava = (obj_LavaArea.y - camera_get_view_y(VIEW))/camera_get_view_height(VIEW) * _height;
	}
	//show_debug_message(str(_ylava)+" "+str(_y1)+" "+str(_y2));
	if (_ylava >= _y1 && _ylava <= _y2) {
		draw_set_alpha(0.4); 
		draw_line_color(_x1, _y1+_ylava, _x2, _y1+_ylava, c_red, c_red);
		draw_set_alpha(1);
	}
	
	type_formatted(GUI_WIDTH-_margin, _margin+_height+15, "[fa_right][fa_middle][fnt_HUD][$CCCCCC]Flowers [$AAAAAA]  "+str(total_score)+"/"+str(total_possible_score));
	type_formatted(GUI_WIDTH-_margin-20, _margin+_height+30, "[fa_right][fa_middle][fnt_HUD][$CCCCCC]SuperJump    ");
	draw_healthbar(GUI_WIDTH-_margin-30, _margin+_height+25, GUI_WIDTH-_margin, _margin+_height+35, 100*(1-obj_Player.alarm[2]/obj_Player.superjump_cooldown), false, c_gray, c_lime, 0, true, true);	
	type_formatted(GUI_WIDTH-_margin, _margin+_height+45, "[fa_right][fa_middle][fnt_HUD][$CCCCCC]M Remaining    "+str(_player_tile_row));
	type_formatted(GUI_WIDTH-_margin, _margin+_height+60, "[fa_right][fa_middle][fnt_HUD][$CCCCCC]Time    "+str(time_format(Game.total_time)),true,false);
	
	if (room == room_Game_1) {
		type_formatted(GUI_WIDTH-_margin, GUI_HEIGHT-_margin, "[fa_right][fa_middle][fnt_HUD][$666666]"+"SEED: "+str(Game.seed));
	}
	else {
		type_formatted(GUI_WIDTH-_margin, GUI_HEIGHT-_margin, "[fa_right][fa_middle][fnt_HUD][$666666]TUTORIAL - ESC TO RETURN TO MENU");
	}
	
	tutorial = [];
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Help a cute wizard [c_aqua]escape[c_white] the volcanic",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]satellite  Io, while saving as many",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_aqua]magical flowers[c_white] as possible [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Use [c_pink]A[c_white] and [c_pink]D[c_white] to [c_aqua]Run[c_white].",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Use [c_pink]W[c_white] to [c_aqua]Jump[c_white] and [c_aqua]Double Jump[c_white].",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_aqua]You can try it now. [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Save a magical flower by [c_aqua]touching[c_white] it.",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]The [c_aqua]minimap[c_white] will show you nearby flowers.",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_aqua]You can try it now. [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]That flower [c_aqua]up[c_white] above too high.",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Use your [c_aqua]SuperJump[c_white] by pressing [c_pink]S[c_white].",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_aqua]You can try it now. [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Your SuperJump has a [c_aqua]cooldown[c_white].",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]The [c_aqua]progress bar[c_white] above will show you",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4]when it's ready. [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]You can also use a gampead:",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white][c_pink]stick[c_white] to move, [c_pink]A[c_white] to jump, [c_pink]B[c_white] to SuperJump.",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]You can try it now. [c_pink](ENTER)"
						]);
	
	array_push(tutorial, [	"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]Try to escape Io and its volcanic",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_white]activity while saving all flowers!",
							"[fa_center][fa_middle][fnt_Menu][scale,0.4][c_aqua](ESC) = Menu, (ENTER) = Stay here. "
						]);
						
	
	var _n_tut = array_length(tutorial);
	var _draw_tutorial = false;
	if (keyboard_check_pressed(vk_enter)) {
		tutorial_index++;
		_draw_tutorial = tutorial_index < _n_tut;
	}
	
	if (room == room_Game_Tutorial && _draw_tutorial) {
		var _margin = 0;
		var _x1 = _margin;
		var _x2 = GUI_WIDTH;
		var _y1 = GUI_HEIGHT/2-100;
		var _y2 = GUI_HEIGHT/2+100;
		//draw_set_color($111111);
		//draw_set_alpha(0.7);	
		//draw_roundrect(_x1, _y1, _x2, _y2, false);
		draw_set_alpha(0.6);
		draw_set_color(c_black);
		draw_rectangle(_x1, _y1, _x2, _y2, false);
		draw_set_color(c_white);
		draw_set_alpha(0.7);	
		draw_rectangle(_x1, _y1, _x2, _y2, true);
		type_formatted( _x1+(_x2-_x1)/2, _y1+(_y2-_y1)/2-40, tutorial[tutorial_index][0]);
		type_formatted( _x1+(_x2-_x1)/2, _y1+(_y2-_y1)/2, tutorial[tutorial_index][1]);
		type_formatted( _x1+(_x2-_x1)/2, _y1+(_y2-_y1)/2+40, tutorial[tutorial_index][2]);
	}
	
	
	fnc_RestoreDrawParams();
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = GUI_MOUSE_X >= _bbox_coords[0] && GUI_MOUSE_Y >= _bbox_coords[1] && GUI_MOUSE_X <= _bbox_coords[2] && GUI_MOUSE_Y <= _bbox_coords[3];
	//var _mouseover = GUI_MOUSE_X >= _draw_data_normal.bbox_x1 && GUI_MOUSE_Y >= _draw_data_normal.bbox_y1 && GUI_MOUSE_X <= _draw_data_normal.bbox_x2 && GUI_MOUSE_Y <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			//script_execute(_callback, _param);
			_callback(_param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	var _function = (asset_get_index("fnc_Menu_"+string(_param)));
	_function();
}

function fnc_ExecuteOptions(_param) {
	var _function = script_execute(asset_get_index("fnc_Options_"+string(_param)));
	_function();
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.5);
		//draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(20, 20,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(20, 30, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.fsm.current_state_name);
		}
		else {
			draw_text(20, 30, "Player not in room");	
		}
		var _spacing = 50;
		
		var _debug_data = [];
		
		array_push(_debug_data, "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")");
		array_push(_debug_data, "Room speed: "+string(room_speed)+" Game speed: "+string(gamespeed_fps));
		array_push(_debug_data, "Requested scaling type: "+string(SELECTED_SCALING)+" ("+
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW ? "Normal mode (resolution scaled to window)" : 
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW ? "Resolution independent of window, unscaled" : "Resolution independent of of window, scaled")+")"));
		array_push(_debug_data, "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H));
		array_push(_debug_data, "Actual base resolution: "+string(adjusted_resolution_width)+"x"+string(adjusted_resolution_height));
		array_push(_debug_data, "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H));
		array_push(_debug_data, "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height));
		array_push(_debug_data, "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")");
		array_push(_debug_data, "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")");
		array_push(_debug_data, "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with GM function): "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with helper functions!): "+string(GUI_WIDTH)+"x"+string(GUI_HEIGHT)+" ("+string(round(GUI_WIDTH/GUI_HEIGHT * 100)/100)+")");
		array_push(_debug_data, "Mouse: "+string(mouse_x)+","+string(mouse_y));
		array_push(_debug_data, "Device mouse 0: "+string(GUI_MOUSE_X)+","+string(GUI_MOUSE_Y));
		array_push(_debug_data, "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0)));
		array_push(_debug_data, "Paused: "+string(paused));
		array_push(_debug_data, "FPS: "+string(fps_real) + "/" + string(fps));
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(20, _spacing+_i*15, _debug_data[_i]);
			//show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion
