// Function to (re)initialize game variables
#region Initialization
	function fnc_InitializeGameStartVariables() {
		paused = false;
		lost = false;
		won = false;
		started = false;
		
		tutorial_index = 0;
		
		level = 1;
		total_score = 0;
		total_possible_score = 0;
		total_time = 0;
		
		current_step = 0;	
	
		scoreboard_queried = false;
		scoreboard = [];
		scoreboard_html5 = [];
		
		http_get_id_query = noone;
		http_return_status_query = noone;
		http_return_result_query = noone;
	
		http_get_id_update = noone;
		http_return_status_update = noone;
		http_return_result_update = noone;
		
		current_scoreboard_updates = 0;
		max_scoreboard_updates = 1;
		timer_scoreboard_updates = 180;
		
		// Reset alarms (except for 0, which is center screen), since Game is persistent
		for (var _i=1; _i<=11; _i++) {
			Game.alarm[_i] = -1;
		}
	
		fullscreen_change = false;
	
		swiping = array_create(4, false);
		swipe_button = mb_right;
		swipe_start = array_create(4, -1);
		swipe_end = array_create(4, -1);
		swipe_distance = array_create(4, -1);
		swipe_direction = array_create(4, -1);
		swipe_time = array_create(4, -1);
		swipe_started_now = array_create(4, false);
		swipe_ended_now = array_create(4, false);
		swipe_in_progress = array_create(4, false);
		swipe_points = array_create(4, []);
		swipe_debug = true;
	
	}
#endregion

#region Helpers
	function time_format(_time) {
		var _secs = floor(_time/60);
		var _millis = _time-_secs*60;
		var _mins = floor(_secs/60);
		var _secs = _secs-_mins*60;
		var _str = string_lpad(str(_mins), "0", 2)+":"+string_lpad(str(_secs), "0", 2)+"."+string_lpad(str(_millis), "0", 2);		
		return _str;
	}

	function string_lpad(_str, _chr, _length) {
		var _string = string_repeat(_chr, _length)+_str;
		return string_reverse(string_copy(string_reverse(_string), 1, _length));
	}
	
	function grid_to_string(_grid) {
		var _str = "";
		var _cols = ds_grid_width(_grid);
		var _rows = ds_grid_height(_grid);
		for (var _row = 0; _row < _rows; _row++) {			
			for (var _col = 0; _col < _cols; _col++) {			
				if (_col == 0) _str += "["+string(_row)+"] ";
				var _chr = "00"+string(_grid[# _col, _row]);
				_str += string_reverse(string_copy(string_reverse(_chr), 1, 2));
				if (_col < _cols-1) _str += " ";				
			}
			_str += "\n";
		}
		return _str;
	}
	
	// Update scoreboard
	function fnc_UpdateScoreboard() {
		if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
			var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/score_insert.php";
			var _hash = sha1_string_utf8(Game.option_value[? Game.option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
			var _params = "user="+Game.option_value[? Game.option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
		
			http_get_id_update = http_get(_scoreboard_url + "?" + _params);
		}
		else {
			http_get_id_update = -1;
		}
	}


	/// @function fnc_ChooseProb(choose_array, array_probs)
	/// @description Chooses a random value from the array with specified probability distribution
	/// @param choose_array The array of values to choose from. If empty array, the function will return the index instead.
	/// @param array_probs The probability array of each values from the list. If empty, use uniform distribution.
	/// @return The chosen element

	function fnc_ChooseProb(_choose_array, _array_probs) {
		var _n_choose = array_length(_choose_array);
		var _n_probs = array_length(_array_probs);
	
		if (_n_probs == 0 && _n_choose == 0) {  // Error
			throw("Error on fnc_ChooseProb, empty arrays provided.");
		}
		else {
			if (_n_probs == 0) {	 // Set uniform distribution
				var _probs = array_create(_n_choose);
				var _k = 0;
				for (var _i=0; _i<_n_choose; _i++) {
					if (_i < _n_choose - 1) {
						_probs[_i] = 1/_n_choose;
						_k=_k+_probs[_i];
					}
					else {
						_probs[_i] = 1-_k;				
					}
				}
			}
			else {	// Use what has been given
				_probs = _array_probs;			
			}
	
			var _rnd = random(1);
			var _i = 0;
			var	_currProb = _probs[_i];
			var _cumProb = _currProb;
			while (_rnd > _cumProb) {
				_i++;
				var	_currProb = _probs[_i];
				var _cumProb = _cumProb + _currProb;
			}
			if (_n_choose == 0) {
				return _i;
			}
			else {		
				return _choose_array[_i];
			}
		}
	}

	/// @function fnc_ChooseProbList(choose_list, array_probs)
	/// @description Chooses a random value from the list with specified probability distribution
	/// @param choose_list The list of values to choose from
	/// @param array_probs The probability array of each values from the list
	/// @return The chosen element

	function fnc_ChooseProbList(_choose_list, _array_probs) {
		var _rnd = random(1);
		var _i = 0;
		var	_currProb = _array_probs[_i];
		var _cumProb = _currProb;
		while (_rnd > _cumProb) {
			_i++;
			var	_currProb = _array_probs[_i];
			var _cumProb = _cumProb + _currProb;
		}
		return _choose_list[| _i];
	}




	/// @function fnc_KeyToString(_key)
	/// @arg _key The keycode to name
	/// @return The reeadable name of the keycode

	function fnc_KeyToString(_key) {
		if (_key >= 48 && _key <= 90) { 
			return chr(_key);
		}
		else {
			switch(_key) {
			    case -1: return "Unassigned";
			    case vk_backspace: return "Backspace";
			    case vk_tab: return "Tab";
			    case vk_enter: return "Enter";
			    case vk_lshift: return "Left Shift";
			    case vk_lcontrol: return "Left Ctrl";
			    case vk_lalt: return "Left Alt";
				case vk_rshift: return "Right Shift";
			    case vk_rcontrol: return "Right Ctrl";
			    case vk_ralt: return "Right Alt";
				case vk_shift: return "Shift";
			    case vk_control: return "Ctrl";
			    case vk_alt: return "Alt";
				case vk_printscreen: return "Print Screen";
			    case vk_pause: return "Pause/Break";
			    case 20: return "Caps Lock";
			    case vk_escape: return "Esc";
				case vk_space: return "Space";
			    case vk_pageup: return "Page Up";
			    case vk_pagedown: return "Page Down";
			    case vk_end: return "End";
			    case vk_home: return "Home";
			    case vk_left: return "Left Arrow";
			    case vk_up: return "Up Arrow";
			    case vk_right: return "Right Arrow";
			    case vk_down: return "Down Arrow";
			    case vk_insert: return "Insert";
			    case vk_delete: return "Delete";
				case vk_divide: return "/";
			    case vk_numpad0: return "Numpad 0";
			    case vk_numpad1: return "Numpad 1";
			    case vk_numpad2: return "Numpad 2";
			    case vk_numpad3: return "Numpad 3";
			    case vk_numpad4: return "Numpad 4";
			    case vk_numpad5: return "Numpad 5";
			    case vk_numpad6: return "Numpad 6";
			    case vk_numpad7: return "Numpad 7";
			    case vk_numpad8: return "Numpad 8";
			    case vk_numpad9: return "Numpad 9";
			    case vk_multiply: return "Numpad *";
			    case vk_add: return "Numpad +";
				case vk_decimal: return "Numpad .";
			    case vk_subtract: return "Numpad -";    
			    case vk_f1: return "F1";
			    case vk_f2: return "F2";
			    case vk_f3: return "F3";
			    case vk_f4: return "F4";
			    case vk_f5: return "F5";
			    case vk_f6: return "F6";
			    case vk_f7: return "F7";
			    case vk_f8: return "F8";
			    case vk_f9: return "F9";
			    case vk_f10: return "F10";
			    case vk_f11: return "F11";
			    case vk_f12: return "F12";
			    case 144: return "Num Lock";
			    case 145: return "Scroll Lock";
			    case ord(";"): return ";";
			    case ord("="): return "=";
			    case ord("\\"): return "\\";    
			    case ord("["): return "[";
			    case ord("]"): return "]";
				default: return "other";
			}
		}
	}

	/// @function fnc_BackupDrawParams
	/// @description Backup the draw parameters to variables

	function fnc_BackupDrawParams() {
		tmpDrawFont = draw_get_font();
		tmpDrawHAlign = draw_get_halign();
		tmpDrawVAlign = draw_get_valign();
		tmpDrawColor = draw_get_color();
		tmpDrawAlpha = draw_get_alpha();	
	}

	/// @function fnc_RestoreDrawParams
	/// @description Restore the draw parameters from variables

	function fnc_RestoreDrawParams() {
		draw_set_font(tmpDrawFont);
		draw_set_halign(tmpDrawHAlign);
		draw_set_valign(tmpDrawVAlign);
		draw_set_color(tmpDrawColor);
		draw_set_alpha(tmpDrawAlpha);	
	}

	///@function			fnc_BaseConvert(string, old_base, new_base)
	///@description			Converts a number [as as string] from an old base to a new base
	///@param	{string}	string		The number expressed as string
	///@param				old_base	The old base
	///@param				new_base	The new base
	///@return				The number [as a string] expressed in the new base
	function fnc_BaseConvert(_string_number, _old_base, _new_base) {
		var number, oldbase, newbase, out;
		number = _string_number;
	    //number = string_upper(_string_number);
	    oldbase = _old_base;
	    newbase = _new_base;
	    out = "";
 
	    var len, tab;
	    len = string_length(number);
	    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
	    var i, num;
	    for (i=0; i<len; i+=1) {
	        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
	    }
 
	    do {
	        var divide, newlen;
	        divide = 0;
	        newlen = 0;
	        for (i=0; i<len; i+=1) {
	            divide = divide * oldbase + num[i];
	            if (divide >= newbase) {
	                num[newlen] = divide div newbase;
	                newlen += 1;
	                divide = divide mod newbase;
	            } else if (newlen  > 0) {
	                num[newlen] = 0;
	                newlen += 1;
	            }
	        }
	        len = newlen;
	        out = string_char_at(tab, divide+1) + out;
	    } until (len == 0);
 
	    return out;
	}


	///@function			in(element, array)
	///@description			Returns whether an element is inside an array
	///@param				element	The required element
	///@param				array	The array to look the element in
	///@return				Boolean that says whether the element is in the array or not
	function in(_element, _array) {
		if (array_length(_array) == 0) {
			return false;
		}
		else {
			var _i=0;
			var _n = array_length(_array);
			var _found = false;
			while (_i<_n && !_found) {
				if (_array[_i] == _element) {
					_found = true;
				}
				else {
					_i++;
				}
			}
		}
		return _found;
	}

	/// @struct				Vec3(x,y,z)
	/// @description		Creates a struct representing a 3D vector
	/// @var	{real}	x	The x coordinate
	/// @var	{real}	y	The y coordinate
	/// @var	{real}	z	The z coordinate
	function Vec3(_x, _y, _z) constructor {
		x = _x;
		y = _y;
		y = _z;
	}



	#region String functions

	/// @function		fnc_StringToList(string, separator, remove_spaces)
	/// @description	Takes a string with separators and returns a DS list of the separated string
	/// @param			string			Source string to split
	/// @param			[separator]		Optional string separator to use. Default: comma
	/// @param			[remove_spaces]	Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
	/// @return			A DS list with the string split into the parts
	function fnc_StringToList(_string, _separator=",", _remove_lead_trail_spaces = false) {
		if (argument_count == 0) {		
			throw ("String argument required and not provided.");
		}
	
		// Process and split
		var _list = ds_list_create();	
		var _substring = _string;
		var _next_separator = string_pos(_separator, _substring);
		while (_next_separator != 0) {
			var _found = string_copy(_substring, 0, _next_separator-1);
			if (_remove_lead_trail_spaces) {			
				_found = fnc_String_lrtrim(_found);
			}
		
			if (string_length(_found) > 0) {
				ds_list_add(_list, _found);		
			}
		
			_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
			var _next_separator = string_pos(_separator, _substring);
		}
		ds_list_add(_list,_substring);
	
		return _list;
	}

	///@function			fnc_String_cleanse(string)
	///@description			Removes non-printable characters from a string 
	///@param				string	The string to process
	///@return				The cleansed string
	function fnc_String_cleanse(_str) {
		var _newstr = _str;
		for (var _j=0; _j<32; _j++) {
			_newstr = string_replace_all(_newstr, chr(_j), "");
		}
		return _newstr;
	}

	///@function			fnc_String_lrtrim(string)
	///@description			Trims spaces before and after the first and last characters
	///@param				string	The string
	///@return				The trimmed string
	function fnc_String_lrtrim(_str) {	
		var _j=0;
		var _l=string_length(_str);
		while (string_copy(_str, _j, 1)==" " && _j<_l) {
			_j++;
		}
		_str = string_copy(_str, _j, _l);
			
		var _j=string_length(_str);
		while (string_copy(_str, _j, 1)==" " && _j>=0) {
			_j--;
		}
		_str = string_copy(_str, 0, _j);
		return _str;	
	}

	#endregion

	/// @struct				Vec2(x,y)
	/// @description		Creates a struct representing a 2D vector
	/// @var	{real}	x	The x coordinate
	/// @var	{real}	y	The y coordinate

	function Vec2(_x, _y) constructor {
		x = _x;
		y = _y;
	
		/// @method						magnitude()
		/// @description				Calculates the 2D vector magnitude divided by a factor
		/// @param	{real}	[factor]	Dividing factor [default=1]
		/// @return	{real}				The normalized 2D vector magnitude
		magnitude = function(factor=1) {
			return sqrt(power(x,2)+power(y,2))/factor;
		}
	
		/// @method						toString()
		/// @description				Prints the 2D vector
		toString = function() {
			return "("+string(x)+","+string(y)+")";	
		}
	}



	function display_get_gui_w() {
		if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
			return Game.adjusted_window_width;
		}
		else {
			return display_get_gui_width();
		}
	}

	function display_get_gui_h() {
		if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
			return Game.adjusted_window_height;
		}
		else {
			return display_get_gui_height();
		}
	}



	/// @function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel)
	/// @description Perform easing with an animation curve with the equation: v = v_ini + lambda * (v_target-v_ini) where lambda changes from 0 to 1 according to the animation curve.
	/// @param _v_ini initial value (when t=0).
	/// @param _v_target target value (when t=1).
	/// @param _t target value
	/// @param _curve animation curve asset.
	/// @param _curve_channel name of the curve channel.
	/// @returns the interpolated value at _t.

	function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel) {
		var _anim_channel = animcurve_get_channel(_curve, _curve_channel);
		var _lambda = animcurve_channel_evaluate(_anim_channel, _t);	
		return _v_ini + _lambda * (_v_target - _v_ini);
	}


	///@function				array_find(array, element)
	///@description				finds element inside array and returns the position, or -1 if not found
	///@param		{array}		array		the array
	///@param		{*}			element		the element to look for
	///@return		{integer}	the 0-based index of element within array, or -1 if it was not found
	function array_find(_things, _array) {
		if (!is_array(_things)) {
			var _lookup_array = [_things];
		}
		else {
			var _lookup_array = _things;
		}
		if (!is_array(_array)) {
			var _target_array = [_array];
		}
		else {
			var _target_array = _array;
		}
		
		var _n = array_length(_target_array);
		var _m = array_length(_lookup_array);
		var _j = 0;
		var _found = false;
		while (_j<_m && !_found) {
			var _i=0;			
			while (_i<_n && !_found) {
				if (_target_array[_i] == _lookup_array[_j]) {
					_found = true;
				}
				else {
					_i++;	
				}
			}
			if (!_found) {
				_j++;
			}
		}
		
		if (_found) {
			return _i;
		}
		else {
			return -1;
		}
	}

	///@function				array_to_string(array)
	///@description				prints an array to log
	///@param		{array}		array		the array
	///@return		{string}	the string representation of the array
	function array_to_string(_array) {
		var _n = array_length(_array);
		var _str = "[";
		for (var _i=0; _i<_n; _i++) {
			if (is_array(_array[_i])) {
				_str += array_to_string(_array[_i]);
			}
			else {
				_str += string(_array[_i]);
			}
			if (_i<_n-1) _str += ",";
		}
		_str += "]";
		return _str;
	}


	function array_resize_from_end(_array, _new_size) {
		var _n = array_length(_array);
		if (_new_size < _n) {		
			for (var _i=0; _i<_new_size; _i++) {
				_array[@_i] = _array[_i+_n-_new_size];
			}
			array_resize(_array, _new_size);
		}
	}


	function convert_gui_to_room(_vec2_gui) {
		// TODO						
	}

	function convert_room_to_gui(_vec2_room) {
		if (ENABLE_LIVE and live_call()) return live_result;
		var _gw = display_get_gui_w();
		var _gh = display_get_gui_h();
		var _rw = room_width;
		var _rh = room_height;
			
		var _sw = surface_get_width(application_surface) * Game.application_surface_scaling;
		var _sh = surface_get_height(application_surface) * Game.application_surface_scaling;
	
		var _cw = camera_get_view_width(VIEW);
		var _ch = camera_get_view_height(VIEW);
		var _cx = camera_get_view_x(VIEW);
		var _cy = camera_get_view_y(VIEW);
	
		var _x_rel_cam = (_vec2_room.x - _cx)/_cw;
		var _y_rel_cam = (_vec2_room.y - _cy)/_ch;

		return new Vec2(_x_rel_cam * _sw + Game.gui_offset_x, _y_rel_cam * _sh + Game.gui_offset_y);

	}



	/// Swipe functions

	#region Swipe functions

	function device_mouse_dragswipe_register(_device) {
		Game.swipe_started_now[_device] = false;
		Game.swipe_ended_now[_device] = false;
	
		if (!Game.swiping[_device]) {
			Game.swiping[_device] = device_mouse_check_button_pressed(_device, Game.swipe_button);
			if (Game.swiping[_device]) { // Swipe started
				Game.swipe_start[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
				Game.swipe_end[_device] = -1;
				Game.swipe_started_now[_device] = true;
				Game.swipe_distance[_device] = -1;
				Game.swipe_direction[_device] = -1;
				Game.swipe_time[_device] = get_timer();		
				Game.swipe_points[_device] = [];
				Game.swipe_in_progress[_device] = true;
			}
		}
		else if (device_mouse_check_button_released(_device, Game.swipe_button)) { // Swipe ended
			Game.swiping[_device] = false;				
			Game.swipe_end[_device] = new Vec2(device_mouse_x(_device), device_mouse_y(_device));
			Game.swipe_ended_now[_device] = true;
			Game.swipe_distance[_device] = point_distance(Game.swipe_start[_device].x, Game.swipe_start[_device].y, Game.swipe_end[_device].x, Game.swipe_end[_device].y);
			Game.swipe_direction[_device] = point_direction(Game.swipe_start[_device].x, Game.swipe_start[_device].y, Game.swipe_end[_device].x, Game.swipe_end[_device].y);
			Game.swipe_time[_device] = (get_timer() - Game.swipe_time[_device])/1000000;		
			Game.swipe_in_progress[_device] = false;
		}
		else {
			// Swiping in progress		
			array_push(Game.swipe_points[_device], new Vec2(device_mouse_x(_device), device_mouse_y(_device)));
			Game.swipe_in_progress[_device] = true;
		}
	}

	function device_mouse_check_dragswipe_start(_device) {
		var _ret = Game.swipe_started_now[_device];
		if (_ret && Game.swipe_debug) show_debug_message("Device "+string(_device)+" Swiping started at "+string(Game.swipe_start[_device]));
		return _ret;	
	}

	function device_mouse_check_dragswipe_end(_device) {	
		var _ret = Game.swipe_ended_now[_device] && Game.swipe_distance[_device] > SWIPE_DETECTION_TOLERANCE;
		if (_ret && Game.swipe_debug) show_debug_message("Device "+string(_device)+" Swiping ended at "+string(Game.swipe_end[_device]));
		return _ret;
	}

	function device_mouse_check_dragswipe(_device) {
		var _ret = Game.swipe_in_progress[_device];
		if (_ret && Game.swipe_debug) show_debug_message("Device "+string(_device)+" Swiping in progress");
		return _ret;
	}

	function device_mouse_dragswipe_distance(_device) {
		var _ret = Game.swipe_distance[_device];
		if (Game.swipe_debug) show_debug_message("Device "+string(_device)+" distance swiped: "+string(_ret));
		return _ret;
	}

	function device_mouse_dragswipe_direction(_device) {
		var _ret = Game.swipe_direction[_device];
		if (Game.swipe_debug) show_debug_message("Device "+string(_device)+" direction swiped: "+string(_ret));
		return _ret;
	}

	function device_mouse_dragswipe_time(_device) {
		var _ret = Game.swipe_time[_device];
		if (Game.swipe_debug) show_debug_message("Device "+string(_device)+" time swiped: "+string(_ret));
		return _ret;	
	}

	function device_mouse_dragswipe_speed(_device) {
		var _ret = Game.swipe_distance[_device]/Game.swipe_time[_device];
		if (Game.swipe_debug) show_debug_message("Device "+string(_device)+" speed swiped: "+string(_ret));
		return _ret;
	}

	#endregion



	///@function				foreach(object, code)
	///@description				Simulates a For Each loop. Iterates over the elements of an array/list/map and executes the passed function, in which the argument name will be used to refer to each item in the loop.
	///@param		{*}			object		The array/list/map
	///@param		{function}	code		A function to be executed, needs an argument and you can use that to refer to the current item.
	function foreach(object, code) {
		var _n, _keys, _thing_;
	
		if (is_string(object)) {
			_n = 1;
		}
		else if (is_array(object)) {
			_n = array_length(object);		
		}
		else if (ds_exists(object, ds_type_list)) {
			_n = ds_list_size(object);
		}
		else if (ds_exists(object, ds_type_map)) {
			_n = ds_map_size(object);
			_keys = ds_map_keys_to_array(object);
		}
		else {
			_n = 1;
		}

		for (var _i=0; _i<_n; _i++) {
			_thing_ = is_string(object) ? object : (is_array(object) ? object[_i] : (ds_exists(object, ds_type_list) ? object[| _i] : (ds_exists(object, ds_type_map) ? object[? _keys[_i]] : object)));
			code(_thing_);
		}
	}



	function approach(_value, _target_value, _amount, _tolerance = 0.0001) {
		if ( abs(_value - _target_value) < _tolerance ) {
			return _target_value;
		}
		else if (_target_value - _value > _tolerance) {
			return min(_target_value, _value + _amount);
		}
		else {
			return max(_target_value, _value - _amount);	
		}
	}

	function gmcallback_fullscreen() {
		ToggleFullScreen();	
	}
#endregion

#region Procedural level generation

	enum TILE {
		UL = 7,
		UM = 8,
		UR = 9,
		ML = 14,
		MM = 15,		
		MR = 16,
		LL = 21,
		LM = 22,
		LR = 23,
		US = 10,
		MS = 17,
		LS = 24,
		TUL = 11,
		TUR = 12,
		TLL = 18,
		TLR = 19
	}
	
	
	
	
	function fnc_GenerateLevel() {
		
		var _time = get_timer();
		
		var _cols_world = ds_grid_width(Game.world);
		var _rows_world = ds_grid_height(Game.world);
		var _cols_platforms = ds_grid_width(Game.world_background);
		var _rows_platforms = ds_grid_height(Game.world_background);
		
		// Initialize
		var _cols = ds_grid_width(Game.world);
		var _rows = ds_grid_height(Game.world);
		for (var _col = 0; _col < _cols; _col++) {
			for (var _row = 0; _row < _rows; _row++) {
				Game.world[# _col, _row] = 0;
				Game.world_background[# _col, _row] = 0;
			}
		}
		
		var _start = 4;
		fnc_GL_GenerateBase(_cols_world, _rows_world, _start); 
		
		_start = _start + 1;
		while (_start < WORLD_HEIGHT_TILES) {
			// ==================== 1) Space
			var _space = irandom_range(0,2);
			_start += _space;
			_start = min(WORLD_HEIGHT_TILES-1, _start);
			
			// ==================== 2) Generate Chunks
						
			// 0 = no world chunks, 1 = left chunk, 2 = mid chunk, 3 = right chunk, 4 = left+right chunk
			var _block_type = fnc_ChooseProb([1, 2, 3, 4], []);

			_h = 0;
			if (_block_type == 1) { // Left
				var _w = irandom_range(3,5);
				var _h = irandom_range(3,4);
				var _row = _start;
				var _col = 0;
				fnc_GL_GenerateChunk(_row, _col, _w, _h);
				//show_debug_message("Left chunk generated at "+str(_row)+" with width "+str(_w)+" height "+str(_h)+" and col "+str(_col));
			}
			if (_block_type == 3) {
				var _w = irandom_range(3,5);
				var _h = irandom_range(3,4);
				var _row = _start;
				var _col = _cols-_w;
				fnc_GL_GenerateChunk(_row, _col, _w, _h);
				//show_debug_message("Right chunk generated at "+str(_row)+" with width "+str(_w)+" height "+str(_h)+" and col "+str(_col));
			}
			if (_block_type == 2) { // Middle
				var _w = irandom_range(3,6);
				var _h = irandom_range(1,3);
				var _row = _start;
				var _col = floor(_cols-choose(4,5,6))/2;
				fnc_GL_GenerateChunk(_row, _col, _w, _h);
				//show_debug_message("Middle chunk generated at "+str(_row)+" with width "+str(_w)+" height "+str(_h)+" and col "+str(_col));
			}
			if (_block_type = 4) {  // Left + Right, make them smaller
				var _w = irandom_range(3,4);
				var _h1 = irandom_range(2,5);
				var _row = _start;
				var _col = 0;
				fnc_GL_GenerateChunk(_row, _col, _w, _h1);
				//show_debug_message("L+R Left chunk generated at "+str(_row)+" with width "+str(_w)+" height "+str(_h)+" and col "+str(_col));
				var _w = irandom_range(3,4);
				var _h2 = irandom_range(2,5);
				var _row = _start;
				var _col = _cols-_w;
				fnc_GL_GenerateChunk(_row, _col, _w, _h2);
				//show_debug_message("L+R Right chunk generated at "+str(_row)+" with width "+str(_w)+" height "+str(_h)+" and col "+str(_col));
				var _h = max(_h1, _h2);
			}

			
			_start = _start + _h;
		}
		
		// ================= 3) Generate platforms
		_start = 5;
		while (_start < WORLD_HEIGHT_TILES-1) {
			// ==================== Space
			var _space = fnc_ChooseProb([0,1],[0.95,0.05]);
			_start += _space;
			_start = min(WORLD_HEIGHT_TILES-1, _start);
			
			var _streak = [-1, -1];
			var _max_streak = [-1, -1];
			_col = 0;
			while (_col < _cols) {
				if (Game.world[# _col, _rows-1-_start] == 0) {
					_streak[0] = _col;					
					while (_col < _cols && Game.world[# _col, _rows-1-_start] == 0) {
						_col++;
					}
					_streak[1] = _col-1;					
					//show_debug_message("Row "+str(_rows-1-_start)+": "+array_to_string(_streak));
					if (_streak[1]-_streak[0] > _max_streak[1]-_max_streak[0]) {
						_max_streak[0] = _streak[0];
						_max_streak[1] = _streak[1];
					}
				}
				else {
					_col++;
				}
			}
			//show_debug_message("Row "+str(_rows-1-_start)+": Max Streak "+array_to_string(_max_streak));
			
			// Only generate platforms if at least 2 spaces in the gap
			_max_streak[0] = _max_streak[0] + 1;
			_max_streak[1] = _max_streak[1] - 1;
			if (_max_streak[1]-_max_streak[0] >= 2) {
				//show_debug_message("Calling platform at row idx "+str(_start));
				fnc_GL_GeneratePlatforms(_start, _max_streak);
			}
			else {
				//show_debug_message("Didn't generate platform at row idx "+str(_start)+" because "+array_to_string(_max_streak));
			}
			
			_start++;
		}
		
		
		_start = 0;
		while (_start < WORLD_HEIGHT_TILES - 1) {
			var _space = irandom_range(3,9);
			_start += _space;
			_start = min(WORLD_HEIGHT_TILES-1, _start);
			var _w = irandom_range(3,6);
			var _h = irandom_range(3,7);
			var _row = _start;
			var _col = irandom_range(2,5);
			fnc_GL_GenerateBackChunk(_row, _col, _w, _h);
			_start++;
		}
		
		//fnc_GL_FixFilledRows();				
		fnc_GL_PositionPlayer(4);		
		fnc_GL_FixClosedAreas();
		fnc_GL_AutoTile(Game.world);
		fnc_GL_AutoTile(Game.world_background);
		fnc_GL_UpdateWorldTile();
		fnc_GL_GenerateFlowers();
		fnc_GL_PositionPlayer(4);
		
		_time = (get_timer() - _time)/1000000;
		show_debug_message("total generation time: "+str(_time)+" sec.");
		
	}
	
	function fnc_GL_FixClosedAreas() {
		var _grid = ds_grid_create(ds_grid_width(Game.world), ds_grid_height(Game.world));
		ds_grid_copy(_grid, Game.world);
		var _cols = ds_grid_width(Game.world);		
		var _player_tile_col = floor(obj_Player.x/TILE_SIZE);
		var _player_tile_row = floor(obj_Player.y/TILE_SIZE);
		
		var _found = fnc_GL_LevelTraversable(_grid);
		var _passes = 0;		
		var _min = [99999999,_player_tile_col];
		
		//show_debug_message("Starting");
		//show_debug_message(grid_to_string(_grid));
		
		while (!_found) {
			_passes++;			
			//show_debug_message("Pass "+str(_passes));
			if (_passes == 1) {
				var _start_col = _player_tile_col;
				var _start_row = _player_tile_row-1;
			}
			else {
				var _start_col = _player_tile_col;
				var _start_row = _min[0]-1;
			}
			fnc_GL_FloodFill(_grid, _start_col, _start_row, _min);
			var _found = fnc_GL_LevelTraversable(_grid);
			//show_debug_message("Level after flood fill and BEFORE clearing:");
			//show_debug_message(grid_to_string(_grid));
			//show_debug_message("Clearing gap in row above "+str(_min[0])+","+str(_min[1]));
			if (!_found)  {
				// Clear a line in the resulting col
				//var _init = _min[1];				
				for (var _i=0; _i<_cols; _i++)  {
					Game.world[# _i, _min[0]-1] = 0;					
				}
				// Copy to temp grid
				ds_grid_copy(_grid, Game.world);
			}
			
			
		}
		
		
		if (!_found) {
			show_debug_message("LEVEL IS IMPOSSIBLE: "+array_to_string(_min));
		}
	}
	
	function fnc_GL_LevelTraversable(_grid) {
		var _col = 0;
		var _cols = ds_grid_width(Game.world);
		var _found = false;
		while (_col < _cols && !_found) {
			if (_grid[# _col, 0] == 99) {
				_found = true;
			}
			else {
				_col++;
			}
		}
		return _found;
	}
	
	function fnc_GL_FloodFill(_grid, _col, _row, _min_array, _allow_28_30 = false) {
		//show_debug_message("Evaluating {"+str(_row)+" "+str(_col)+" with "+str(_allow_28_30));
		var _cols = ds_grid_width(_grid);
		var _rows = ds_grid_height(_grid);
		var _stop_condition;
		
		if (_cols < 0 || _col >= _cols || _row < 0 || _row >= _rows) {
			return;
		}
		else {
			if (_allow_28_30) {
				//show_debug_message("  value is = "+str(_grid[# _col, _row]));
				_stop_condition = _grid[# _col, _row] != 0 && _grid[# _col, _row] != 28 && _grid[# _col, _row] != 30 && _grid[# _col, _row] != 99;
			}
			else {
				_stop_condition = _grid[# _col, _row] != 0;
			}	
			
			if (_stop_condition) {
				return;
			}
			else {
				_grid[# _col, _row] = 99;
				_min_array[@ 0] = min(_min_array[0], _row);
				_min_array[@ 1] = _col;
				fnc_GL_FloodFill(_grid, _col-1, _row, _min_array);
				fnc_GL_FloodFill(_grid, _col, _row-1, _min_array, true);
				fnc_GL_FloodFill(_grid, _col+1, _row, _min_array);
				fnc_GL_FloodFill(_grid, _col, _row+1, _min_array);
			}
		}
	}
	
	function fnc_GL_FixFilledRows() {
		var _cols = ds_grid_width(Game.world);
		var _rows = ds_grid_height(Game.world);
		// Single rows
		for (var _row = 0; _row < _rows; _row++) {
			var _filled = true;
			for (var _col = 0; _col<_cols; _col++) {
				_filled = _filled && Game.world[# _col, _row];
			}
			if (_filled) {
				var _gap_start = irandom_range(3, _cols-3);
				var _gap_end = _gap_start + irandom_range(2,3);
				for (var _g = _gap_start; _g<=_gap_end; _g++) {
					Game.world[# _g, _row] = 0;					
				}
				//show_debug_message("Fixed row "+str(_row)+" by gap ["+str(_gap_start)+","+str(_gap_end)+"]");
			}
		}
		
		// Double rows
		_row = 0; 
		_gap_start = -1;
		_gap_end = -1;
		while (_row < _rows-1) {			
			var _filled = true;
			for (var _col = 0; _col<_cols; _col++) {
				_filled = _filled && (Game.world[# _col, _row] || Game.world[# _col, _row+1]);
			}
			if (_filled) {
				var _gap_start = irandom_range(3, _cols-3);
				var _gap_end = _gap_start + irandom_range(2,3);
				for (var _g = _gap_start; _g<=_gap_end; _g++) {
					if (Game.world[# _g, _row] != 0) {
						Game.world[# _g, _row] = 0;					
					}
					else {
						Game.world[# _g, _row+1] = 0;
					}
				}
			}
			
			//show_debug_message("Double row fixed row "+str(_row)+" by gap ["+str(_gap_start)+","+str(_gap_end)+"]");
			_row++;
		}
	}
	
	function fnc_GL_GenerateFlowers() {
		var _rows = ds_grid_height(Game.world);
		var _cols = ds_grid_width(Game.world);
		
		for (var _row = 1; _row < _rows-1; _row++) {
			for (var _col = 1; _col < _cols-1; _col++)  {
				var _actual_row = _rows-_row-1;
				
				if (Game.world[# _col-1, _actual_row+1] != 0 &&
					Game.world[# _col, _actual_row+1] != 0 &&
					Game.world[# _col+1, _actual_row+1] != 0 &&
					random(1) < 0.4) {
					var _id = instance_create_layer(TILE_SIZE*_col+TILE_SIZE/2, TILE_SIZE*(_actual_row+1)+2*MINI_TILE_SIZE,"lyr_Instances_Behind", obj_MossyHill);
					with (_id) {
						image_xscale = choose(1,-1);
						sprite_index = choose(spr_MossyHill_1,spr_MossyHill_2,spr_MossyHill_3);						
					}
				}
				
				if (Game.world[# _col-1, _actual_row+1] != 0 &&
					Game.world[# _col, _actual_row+1] != 0 &&
					Game.world[# _col+1, _actual_row+1] != 0 &&
					random(1) < 0.5) {
					var _id = instance_create_layer(TILE_SIZE*_col+TILE_SIZE/2, TILE_SIZE*(_actual_row+1)+2*MINI_TILE_SIZE,"lyr_Instances_Behind", obj_Rock);
					with (_id) {
						image_xscale = choose(1,-1)*0.4;
						image_yscale = 0.4;
						sprite_index = fnc_ChooseProb([spr_Rock_1,spr_Rock_2,spr_Rock_3],[0.4,0.4,0.2]);
					}
				}
				
				if (Game.world[# _col, _actual_row] == 0 &&
					Game.world[# _col, _actual_row-1] == 0 &&
					Game.world[# _col-1, _actual_row-1] == 0 &&
					Game.world[# _col+1, _actual_row-1] == 0 &&
					Game.world[# _col-1, _actual_row+1] != 0 &&
					Game.world[# _col, _actual_row+1] != 0 &&
					Game.world[# _col+1, _actual_row+1] != 0
					) {
					
					if (random(1)<0.8) {
						var _id = instance_create_layer(TILE_SIZE*_col+TILE_SIZE/2, TILE_SIZE*(_actual_row+1)+2*MINI_TILE_SIZE,"lyr_Instances_Behind", obj_Plant);
						with (_id) {
							image_xscale = choose(1,-1);
							sprite_index = choose(spr_Plant_1, spr_Plant_2, spr_Plant_3);
						}
					}
					
					if (random(1) < 0.75) {
						var _id = instance_create_layer(TILE_SIZE*_col+TILE_SIZE/2, TILE_SIZE*(_actual_row+1)+2*MINI_TILE_SIZE,"lyr_MagicFlowers", obj_BlueFlower);
						with (_id) {
							image_xscale = choose(1,-1);
							sprite_index = spr_BlueFlower_Closed;
						}
					}
					
					
					//show_debug_message(str(Game.world[# _col-1, _actual_row+1])+" "+str(Game.world[# _col, _actual_row+1])+" "+str(Game.world[# _col+1, _actual_row+1]));
					//show_debug_message("Generated flower at "+str(TILE_SIZE*_col+TILE_SIZE/2)+","+str(TILE_SIZE*(_actual_row+1)+2*MINI_TILE_SIZE)+" (tile="+str(_actual_row+1)+","+str(_col)+")");
						
					//_col = _cols; // End row
				}
				
			}
		}
	}
	
	function fnc_GL_GeneratePlatforms(_start_row, _streak_array) {
		var _rows = ds_grid_height(Game.world);
		var _width = _streak_array[1]-_streak_array[0]+1;		
		
		for (var _col=_streak_array[0]; _col<=_streak_array[1]; _col++) {
			if (random(1) < 1) {
				var _size = clamp(fnc_ChooseProb([1,2,3],[0.6,0.3,0.1]), 0, _streak_array[1]-_col+1);
				var _row = _rows-1-_start_row;
				
				var _check = true;
				for (var _i=0; _i<_size; _i++) {
					var _c = _col+_i;
					_check = _check &&	Game.world[# _c-1, _row-1] == 0 && 
										Game.world[# _c, _row-1] == 0 && 
										Game.world[# _c+1, _row-1] == 0 && 
										Game.world[# _c-1, _row] == 0 && 
										Game.world[# _c+1, _row] == 0 && 
										Game.world[# _c-1, _row+1] == 0 && 
										Game.world[# _c, _row+1] == 0 && 
										Game.world[# _c+1, _row+1] == 0
				}
				if (_check) {
					for (var _i=0; _i<_size; _i++) {
						Game.world[# _col+_i, _row] = TILE.MM;
					}
					//show_debug_message(" Placed "+str(_size)+" platform block at "+str(_row)+","+str(_col));
				}
			}
		}
				
	}
	
	function fnc_GL_GenerateChunk(_start_row, _start_col, _width, _height) {
		var _rows = ds_grid_height(Game.world);
		var _cols = ds_grid_width(Game.world);
		var _offset = fnc_ChooseProb([-2, -1, 0, 1, 2], [0.2, 0.25, 0.1, 0.25, 0.2]);
		for (var _row = _start_row; _row < min(_start_row+_height, _rows); _row++) {			
			var _this_width = clamp(_width + _offset, 1, _cols-1);
			
			for (var _col = _start_col; _col < min(_start_col+_this_width, _cols); _col++) {				
				if (_row == _start_row || _row == min(_start_row+_height, _rows) - 1) { 
					if (random(1) < 0.8) { // small gaps in first and last row
						Game.world[# _col, _rows-1-_row] = TILE.MM;
					}
				}
				else {
					Game.world[# _col, _rows-1-_row] = TILE.MM;
				}
			}
			var _offset = fnc_ChooseProb([-2, -1, 0, 1, 2], [0.1, 0.25, 0.3, 0.25, 0.1]);
		}
	}
	
	function fnc_GL_GenerateBackChunk(_start_row, _start_col, _width, _height) {
		var _rows = ds_grid_height(Game.world_background);
		var _cols = ds_grid_width(Game.world_background);
		var _offset = fnc_ChooseProb([-2, -1, 0, 1, 2], [0.2, 0.25, 0.1, 0.25, 0.2]);
		for (var _row = _start_row; _row < min(_start_row+_height, _rows); _row++) {			
			var _this_width = clamp(_width + _offset, 1, _cols-1);
			
			for (var _col = _start_col; _col < min(_start_col+_this_width, _cols); _col++) {				
				if (_row == _start_row || _row == min(_start_row+_height, _rows) - 1) { 
					if (random(1) < 0.8) { // small gaps in first and last row
						Game.world_background[# _col, _rows-1-_row] = TILE.MM;
					}
				}
				else {
					Game.world_background[# _col, _rows-1-_row] = TILE.MM;
				}
			}
			var _offset = fnc_ChooseProb([-2, -1, 0, 1, 2], [0.1, 0.25, 0.3, 0.25, 0.1]);
		}
	}
	
	function fnc_GL_GenerateBase(_cols_world, _rows_world, _base_height) {
		var _gap_start = irandom_range(2, 4);		
		var _gap_end = clamp(_gap_start + irandom_range(2, 3) - 1, 0, 6);
		var _gap_width = _gap_end - _gap_start + 1;
		//show_debug_message("Gap from "+str(_gap_start)+" to "+str(_gap_end)+ " (width="+str(_gap_width)+")");
		var _offset_start = 0;
		var _offset_end = 0;
		for (var _row = 0; _row<_base_height; _row++) {	
			if (_row > 0) {
				var _rnd = random(1);
				if (_rnd<0.5) _offset_start = max(0, _offset_start-1);
				else if (_rnd<0.8) _offset_start = min(_offset_start+1, _offset_end - 1);
				var _rnd = random(1);
				if (_rnd<0.3) _offset_end = max(_offset_end-1, _offset_start+1);
				else if (_rnd<0.8) _offset_end = min(_offset_end+1, _gap_end-1);
			}
			//show_debug_message(str(_row)+":   Offset start: "+str(_offset_start)+"   Offset end: "+str(_offset_end));
			//show_debug_message(str(_row)+":   "+str(_gap_start+_offset_start)+"   "+str(_gap_end+_offset_end));
			for (var _col = 0; _col < _cols_world; _col++) {				
				if (_col <_gap_start+_offset_start || _col > _gap_end + _offset_end) {
					Game.world[# _col, _rows_world-1-_row] = TILE.MM					
				}
			}			
		}
		//Game.world[# 0, _rows_world-TILES_PER_SCREEN] = TILE.LL;
	}
	
	function fnc_GL_UpdateWorldTile() {
		var _cols = ds_grid_width(Game.world);
		var _rows = ds_grid_height(Game.world);
		var _layer_world = layer_tilemap_get_id(layer_get_id("lyr_Tile_World"));
		var _layer_background = layer_tilemap_get_id(layer_get_id("lyr_Tile_Background"));
		var _layer_collision = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
		
		// Border
		for (var _col=0; _col<room_width/MINI_TILE_SIZE; _col++) {
			for (var _row=0; _row<room_height/MINI_TILE_SIZE; _row++) {
				if (_col == 0 || _col == room_width/MINI_TILE_SIZE-1) {
					tilemap_set(_layer_collision, 1, _col, _row);
				}
			}
		}
		
		
		for (var _col = 0; _col < _cols; _col++) {
			for (var _row = _rows-TILES_PER_SCREEN; _row < _rows; _row++) {
				if (Game.world_background[# _col, _row] != 0) {
					tilemap_set(_layer_background, Game.world_background[# _col, _row], _col, _row-(_rows-TILES_PER_SCREEN));
				}
				if (Game.world[# _col, _row] != 0) {
					tilemap_set(_layer_world, Game.world[# _col, _row], _col, _row-(_rows-TILES_PER_SCREEN));
					if (	Game.world[# _col, _row] == 7 ||
							Game.world[# _col, _row] == 8 ||
							Game.world[# _col, _row] == 9 ||
							Game.world[# _col, _row] == 10 ||
							Game.world[# _col, _row] == 28 ||
							Game.world[# _col, _row] == 29 ||
							Game.world[# _col, _row] == 30 ||
							Game.world[# _col, _row] == 31 ||
							Game.world[# _col, _row] == 37 ||
							Game.world[# _col, _row] == 38 ||
							Game.world[# _col, _row] == 39 ||
							Game.world[# _col, _row] == 40 ||
							Game.world[# _col, _row] == 41) {
								var _collision_start_row = 2;
							}
					else {
						var _collision_start_row = 0;
					}
					for (var _i=_collision_start_row; _i<TILE_SIZE/MINI_TILE_SIZE; _i++) {
						if (Game.world[# _col, _row] == 28) {
							var _collision_start_col = (TILE_SIZE/MINI_TILE_SIZE)/2;
							var _collision_end_col = TILE_SIZE/MINI_TILE_SIZE;
						}
						else if (Game.world[# _col, _row] == 30) {
							var _collision_start_col = 0;
							var _collision_end_col = (TILE_SIZE/MINI_TILE_SIZE)/2;
						}
						else {
							var _collision_start_col = 0;
							var _collision_end_col = TILE_SIZE/MINI_TILE_SIZE;
						}
						for (var _j=_collision_start_col; _j<_collision_end_col; _j++) {							
							tilemap_set(_layer_collision, 1, _col*TILE_SIZE/MINI_TILE_SIZE+_j, (_row-(_rows-TILES_PER_SCREEN))*TILE_SIZE/MINI_TILE_SIZE+_i);
						}
					}
				}
				/*else {
					tilemap_set(_layer_world, 31, _col, _row-(_rows-TILES_PER_SCREEN));
				}*/
			}
		}
	}
	
	function fnc_GL_PositionPlayer(_base_height) {
		var _col = 5;
		var _rows = ds_grid_height(Game.world);
		var _sign = choose(1, -1);
		var _i = 1;
		var _row = _rows - _base_height - 1;
		
		while (!(Game.world[# _col, _row+1] != 0 && Game.world[# _col, _row+1] != 28 && Game.world[# _col, _row+1] != 30 && Game.world[# _col, _row] == 0)) {
			_col = _col + _i*_sign;
			_sign = _sign * -1;
			_i++;
		}
		//show_debug_message("at row "+str(_row)+" checking "+str(_row+1)+" "+str(_col)+" is start");
		obj_Player.x = TILE_SIZE * _col + TILE_SIZE/2;
		obj_Player.y = room_height - _base_height * TILE_SIZE;
	}
	
	function fnc_GL_AutoTile(_grid) {
		var _rows = ds_grid_height(_grid);
		var _cols = ds_grid_width(_grid);
		for (var _col = 0; _col < _cols; _col++) {
			for (var _row = _rows-TILES_PER_SCREEN; _row < _rows; _row++) {
				if (_grid[# _col, _row] != 0) {
					_grid[# _col, _row] = fnc_GL_AutoTile_Tile(_grid, _col, _row);
				}
			}
		}
	}
	
	#region Autotile Position
		function fnc_GL_AutoTile_Tile(_grid, _col, _row) {
			var _empty = array_create(9,0);
			_empty[0] = fnc_GL_ValidPos(_grid, _col-1, _row-1) ? _grid[# _col-1, _row-1] != 0 : true;
			_empty[1] = fnc_GL_ValidPos(_grid, _col, _row-1) ? _grid[# _col, _row-1] != 0 : false;
			_empty[2] = fnc_GL_ValidPos(_grid, _col+1, _row-1) ? _grid[# _col+1, _row-1] != 0 : true;
			_empty[3] = fnc_GL_ValidPos(_grid, _col-1, _row) ? _grid[# _col-1, _row] != 0 : true;
			_empty[4] = true;
			_empty[5] = fnc_GL_ValidPos(_grid, _col+1, _row) ? _grid[# _col+1, _row] != 0 : true;
			_empty[6] = fnc_GL_ValidPos(_grid, _col-1, _row+1) ? _grid[# _col-1, _row+1] != 0 : true;
			_empty[7] = fnc_GL_ValidPos(_grid, _col, _row+1) ? _grid[# _col, _row+1] != 0 : true;
			_empty[8] = fnc_GL_ValidPos(_grid, _col+1, _row+1) ? _grid[# _col+1, _row+1] != 0 : true;
			var _str = array_to_string(_empty);
			//show_debug_message("AT "+str(_col)+","+str(_row)+": "+_str);
			var _ret = 0;
					  
			switch (_str) {
				
				case "[0,0,0,0,1,1,0,1,1]":				
				case "[0,0,0,0,1,1,1,1,1]":	
				case "[0,0,1,0,1,1,0,1,0]": 
				case "[0,0,1,0,1,1,0,1,1]": 
				case "[0,0,1,0,1,1,1,1,0]":
				case "[1,0,1,0,1,1,0,1,1]":
				case "[1,0,0,0,1,1,0,1,1]":
				case "[1,0,0,0,1,1,1,1,0]":
				case "[1,0,0,0,1,1,1,1,1]":
				case "[0,0,1,0,1,1,1,1,1]": _ret = 7; break;
				case "[0,0,0,1,1,0,0,1,0]":	
				
				case "[0,0,0,1,1,0,1,1,0]":	
				case "[0,0,0,1,1,0,1,1,1]":	
				case "[1,0,0,1,1,0,0,1,0]":	
				
				case "[1,0,0,1,1,0,1,1,0]":	
				
				case "[0,0,1,1,1,0,1,1,1]":
				case "[1,0,0,1,1,0,1,1,1]":
				case "[0,0,1,1,1,0,1,1,0]":
				case "[1,0,1,1,1,0,1,1,0]":
				case "[1,0,1,1,1,0,0,1,1]":
				case "[1,0,1,1,1,0,1,1,1]": _ret = 9; break;				
				
				case "[0,1,1,0,1,1,0,0,0]":	
				case "[0,1,1,0,1,1,0,0,1]":	
				
				case "[1,1,0,0,1,1,0,0,1]":	
				case "[1,1,1,0,1,1,0,0,0]":	
				case "[0,1,1,0,1,1,1,0,0]":
				case "[0,1,1,0,1,1,1,0,1]":
				case "[1,1,1,0,1,1,1,0,0]":
				case "[1,1,0,1,1,0,0,0,1]":
				case "[1,1,1,0,1,1,1,0,1]":
				case "[1,1,1,0,1,1,0,0,1]":	_ret = 21; break;
				
				case "[0,1,0,1,1,0,1,0,0]":
				case "[0,1,1,1,1,0,1,0,1]":
				case "[0,1,1,1,1,0,1,0,0]":
				case "[1,1,0,1,1,0,0,0,0]":
				case "[1,1,0,1,1,0,1,0,0]":
				case "[1,1,1,1,1,0,0,0,0]":
				case "[1,1,0,1,1,0,1,0,1]":
				case "[1,1,1,1,1,0,0,0,1]":
				case "[1,1,1,1,1,0,1,0,1]":
				case "[0,1,1,1,1,0,0,0,1]":
				case "[1,1,1,1,1,0,1,0,0]": _ret = 23; break;
				
				case "[0,0,0,1,1,1,0,1,1]":
				
				case "[0,0,0,1,1,1,1,1,1]":
				
				
				case "[0,0,1,1,1,1,1,1,0]":
				case "[0,0,1,1,1,1,1,1,1]":
				
				
				
				case "[1,0,0,1,1,1,1,1,1]":
				case "[1,0,1,1,1,1,0,1,0]":
				
				
				case "[1,0,1,1,1,1,1,1,1]": _ret = 8; break;
				
				case "[0,1,0,1,1,1,0,0,1]":
				
				
				
				
				case "[1,1,1,1,1,1,0,0,0]":
				case "[1,1,1,1,1,1,0,0,1]":
				case "[1,1,1,1,1,1,1,0,0]":
				case "[1,1,1,1,1,1,1,0,1]": _ret = 22; break;				
				case "[0,1,0,0,1,1,1,1,0]":
					
				
				case "[0,1,1,0,1,1,0,1,1]":
				
				case "[0,1,1,0,1,1,1,1,1]":
				case "[1,1,0,0,1,1,0,1,0]":
				
				case "[1,1,0,0,1,1,1,1,0]":
				
				case "[1,1,1,0,1,1,0,1,0]":
				case "[1,1,1,0,1,1,0,1,1]":
				
				
				case "[1,1,1,0,1,1,1,1,1]": _ret = 14; break;
				
				case "[0,1,0,1,1,0,0,1,1]":
				
				
				case "[0,1,1,1,1,0,0,1,0]":
				
				case "[0,1,1,1,1,0,1,1,0]":
				
				
				
				case "[1,1,0,1,1,0,1,1,0]":
				case "[1,1,0,1,1,0,1,1,1]":
				
				
				case "[1,1,1,1,1,0,1,1,0]":
				case "[1,1,1,1,1,0,1,1,1]": _ret = 16; break;
				case "[0,0,0,0,1,0,0,1,0]":
				case "[0,0,0,0,1,0,0,1,1]":
				case "[0,0,0,0,1,0,1,1,0]":
				case "[0,0,0,0,1,0,1,1,1]":
				case "[0,0,1,0,1,0,0,1,0]":
				case "[0,0,1,0,1,0,0,1,1]":
				case "[0,0,1,0,1,0,1,1,0]":
				case "[0,0,1,0,1,0,1,1,1]":
				case "[1,0,0,0,1,0,0,1,0]":
				case "[1,0,0,0,1,0,0,1,1]":
				case "[1,0,0,0,1,0,1,1,0]":
				case "[1,0,0,0,1,0,1,1,1]":
				case "[1,0,1,0,1,0,0,1,0]":
				case "[1,0,1,0,1,0,0,1,1]":
				case "[1,0,1,0,1,0,1,1,0]":
				case "[1,0,1,0,1,0,1,1,1]": _ret = 10; break;
				case "[0,1,0,0,1,0,0,0,0]":
				case "[0,1,0,0,1,0,0,0,1]":
				case "[0,1,0,0,1,0,1,0,0]":
				case "[0,1,0,0,1,0,1,0,1]":
				case "[0,1,1,0,1,0,0,0,0]":
				case "[0,1,1,0,1,0,0,0,1]":
				case "[0,1,1,0,1,0,1,0,0]":
				case "[0,1,1,0,1,0,1,0,1]":
				case "[1,1,0,0,1,0,0,0,0]":
				case "[1,1,0,0,1,0,0,0,1]":
				case "[1,1,0,0,1,0,1,0,0]":
				case "[1,1,0,0,1,0,1,0,1]":
				case "[1,1,1,0,1,0,0,0,0]":
				case "[1,1,1,0,1,0,0,0,1]":
				case "[1,1,1,0,1,0,1,0,0]":
				case "[1,1,1,0,1,0,1,0,1]": _ret = 24; break;
				case "[0,0,0,1,1,0,0,0,0]":
				case "[0,0,0,1,1,0,0,0,1]":
				case "[0,0,0,1,1,0,1,0,0]":
				case "[0,0,0,1,1,0,1,0,1]":
				case "[0,0,1,1,1,0,0,0,0]":
				case "[0,0,1,1,1,0,0,0,1]":
				case "[0,0,1,1,1,0,1,0,0]":
				case "[0,0,1,1,1,0,1,0,1]":
				case "[1,0,0,1,1,0,0,0,0]":
				case "[1,0,0,1,1,0,0,0,1]":
				case "[1,0,0,1,1,0,1,0,0]":
				case "[1,0,0,1,1,0,1,0,1]":
				case "[1,0,1,1,1,0,0,0,0]":
				case "[1,0,1,1,1,0,0,0,1]":
				case "[1,0,1,1,1,0,1,0,0]":
				case "[1,0,1,1,1,0,1,0,1]": _ret = 30; break;
				case "[0,0,0,0,1,1,0,0,0]":
				case "[0,0,0,0,1,1,0,0,1]":
				case "[0,0,0,0,1,1,1,0,0]":
				case "[0,0,0,0,1,1,1,0,1]":
				case "[0,0,1,0,1,1,0,0,0]":
				case "[0,0,1,0,1,1,0,0,1]":
				case "[0,0,1,0,1,1,1,0,0]":
				case "[0,0,1,0,1,1,1,0,1]":
				case "[1,0,0,0,1,1,0,0,0]":
				case "[1,0,0,0,1,1,0,0,1]":
				case "[1,0,0,0,1,1,1,0,0]":
				case "[1,0,0,0,1,1,1,0,1]":
				case "[1,0,1,0,1,1,0,0,0]":
				case "[1,0,1,0,1,1,0,0,1]":
				case "[1,0,1,0,1,1,1,0,0]":
				
				
				case "[1,0,1,0,1,1,1,0,1]": _ret = 28; break;				
				
				case "[0,0,0,1,1,1,1,1,0]": 
				case "[1,0,1,1,1,1,1,1,0]":
				case "[1,0,0,1,1,1,1,1,0]": _ret = 37; break;
				case "[0,0,1,1,1,1,0,1,1]": 
				case "[1,0,0,1,1,1,0,1,1]": 
				case "[1,0,1,1,1,1,0,1,1]": _ret = 38; break;
				case "[1,1,0,1,1,1,0,0,0]": 
				case "[1,1,0,1,1,1,0,0,1]": 
				case "[1,1,0,1,1,1,1,0,0]": 
				case "[1,1,0,1,1,1,1,0,1]": _ret = 44; break;
				case "[0,1,1,1,1,1,0,0,0]": 
				case "[0,1,1,1,1,1,1,0,0]": 
				case "[0,1,1,1,1,1,0,0,1]": 
				case "[0,1,1,1,1,1,1,0,1]": _ret = 45; break;
				
				
				case "[1,1,1,1,1,1,1,1,0]": _ret = 11; break;
				case "[1,1,1,1,1,1,0,1,1]": _ret = 12; break;
				case "[1,1,0,1,1,1,1,1,1]": _ret = 18; break;
				case "[0,1,1,1,1,1,1,1,1]": _ret = 19; break;
				
				case "[0,1,0,1,1,1,1,1,1]": _ret = 13; break;
				case "[0,1,1,1,1,1,0,1,1]": _ret = 20; break;
				case "[1,1,0,1,1,1,1,1,0]": _ret = 27; break;
				case "[1,1,1,1,1,1,0,1,0]": _ret = 34; break;
				
				case "[0,0,0,0,1,0,0,0,0]": 
				case "[0,0,0,0,1,0,0,0,1]": 
				case "[0,0,0,0,1,0,1,0,0]": 
				case "[0,0,0,0,1,0,1,0,1]": 
				case "[0,0,1,0,1,0,0,0,0]": 
				case "[0,0,1,0,1,0,0,0,1]": 
				case "[0,0,1,0,1,0,1,0,0]": 
				case "[0,0,1,0,1,0,1,0,1]": 
				case "[1,0,0,0,1,0,0,0,0]": 
				case "[1,0,0,0,1,0,0,0,1]": 
				case "[1,0,0,0,1,0,1,0,0]": 
				case "[1,0,0,0,1,0,1,0,1]": 
				case "[1,0,1,0,1,0,0,0,0]": 
				case "[1,0,1,0,1,0,0,0,1]": 
				case "[1,0,1,0,1,0,1,0,0]": 
				case "[1,0,1,0,1,0,1,0,1]": _ret = 31; break;
				case "[0,1,1,0,1,0,1,1,1]":
				case "[0,1,0,0,1,0,0,1,0]": 
				case "[0,1,1,0,1,0,1,1,0]":
				case "[0,1,1,0,1,0,0,1,0]":
				case "[0,1,0,0,1,0,0,1,1]":
				case "[0,1,0,0,1,0,1,1,1]":
				case "[1,1,1,0,1,0,0,1,1]":
				case "[0,1,0,0,1,0,1,1,0]":
				case "[1,1,0,0,1,0,1,1,1]":
				case "[1,1,0,0,1,0,1,1,0]":
				case "[0,1,1,0,1,0,0,1,1]": _ret = 17; break;
				case "[0,0,0,1,1,1,0,0,0]": 
				case "[0,0,0,1,1,1,0,0,1]": 
				case "[1,0,0,1,1,1,1,0,0]":
				case "[0,0,0,1,1,1,1,0,0]":
				case "[1,0,0,1,1,1,0,0,0]":
				case "[0,0,1,1,1,1,1,0,1]":
				case "[1,0,1,1,1,1,0,0,0]":
				case "[1,0,1,1,1,1,0,0,1]":
				case "[1,0,1,1,1,1,1,0,1]":
				case "[1,0,1,1,1,1,1,0,0]":
				case "[1,0,0,1,1,1,1,0,1]":
				case "[0,0,1,1,1,1,0,0,0]":
				case "[0,0,0,1,1,1,1,0,1]":
				case "[1,0,0,1,1,1,0,0,1]":
				case "[0,0,1,1,1,1,1,0,0]":
				case "[0,0,1,1,1,1,0,0,1]": _ret = 29; break;
				
				case "[0,1,0,1,1,1,1,1,0]": _ret = 25; break;
				case "[0,1,0,0,1,1,0,1,1]":
				case "[1,1,0,0,1,1,1,1,1]": 
				case "[0,1,0,0,1,1,1,1,1]": // Potential problem... previously 53
				case "[1,1,0,0,1,1,0,1,1]": _ret = 42; break;
				
				case "[0,0,0,0,1,1,0,1,0]":	
				case "[0,0,0,0,1,1,1,1,0]":	_ret = 39; break;
				
				case "[0,1,0,0,1,1,0,1,0]":	_ret = 46; break;
				
				case "[0,1,0,1,1,1,0,0,0]": 
				case "[0,1,0,1,1,1,1,0,1]":
				case "[0,1,0,1,1,1,1,0,0]": _ret = 54; break;
				case "[0,1,0,1,1,0,1,1,0]": 
				case "[0,1,1,1,1,0,1,1,1]": 
				case "[0,1,0,1,1,0,1,1,1]": _ret = 43; break;
				case "[1,1,0,1,1,0,0,1,1]":
				case "[1,1,1,1,1,0,0,1,1]": 
				case "[1,1,1,1,1,0,0,1,0]": 
				case "[1,1,0,1,1,0,0,1,0]": _ret = 36; break;
				case "[0,1,1,1,1,1,1,1,0]": _ret = 52; break;
				case "[0,1,0,1,1,0,0,1,0]": 
				case "[1,0,1,1,1,0,0,1,0]":
				case "[0,0,0,1,1,0,0,1,1]":	
				case "[1,0,0,1,1,0,0,1,1]":	_ret = 41; break;
				case "[1,0,0,1,1,1,0,1,0]": 
				case "[0,0,0,1,1,1,0,1,0]": 
				case "[0,0,1,1,1,1,0,1,0]": _ret = 40; break;
				case "[0,1,0,0,1,1,0,0,1]":	
				
				case "[0,1,0,0,1,1,0,0,0]":	
				case "[1,1,0,0,1,1,0,0,0]":	
				case "[1,1,0,0,1,1,1,0,0]": _ret = 53; break;
				case "[0,1,1,0,1,1,0,1,0]": 
				case "[1,1,1,0,1,1,1,1,0]": 
				case "[0,1,1,0,1,1,1,1,0]": _ret = 35; break;
				case "[0,1,1,1,1,0,0,0,0]":
				case "[0,1,0,1,1,0,0,0,0]": _ret = 55; break;
				case "[1,1,1,0,1,0,1,1,1]": _ret = 17; break;
				//case "[1,1,1,1,1,1,1,1,1]": _ret = 47; break;
				case "[1,1,0,1,1,1,0,1,1]": _ret = 51; break;
				case "[1,1,0,1,1,1,0,1,0]": _ret = 32; break;
				case "[0,1,1,1,1,0,0,1,1]": _ret = 48; break;
				case "[1,1,1,1,1,1,1,1,1]": _ret = fnc_ChooseProb([49, 50, 15], [0.02, 0.03, 0.95]); break;
				default:					_ret = 15; break;
			}
			return _ret;
		}	
	#endregion
	
	function fnc_GL_ValidPos(_grid, _col, _row) {
		return !(_col < 0 || _col >= ds_grid_width(_grid) || _row < 0 || _row >= ds_grid_height(_grid));
	}
	
#endregion