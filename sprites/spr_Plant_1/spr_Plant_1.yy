{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 33,
  "bbox_right": 111,
  "bbox_top": 31,
  "bbox_bottom": 108,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dc0c512e-2149-4f6a-8462-32bea150ea5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dc0c512e-2149-4f6a-8462-32bea150ea5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"dc0c512e-2149-4f6a-8462-32bea150ea5b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"11f3c460-eb24-42fa-baca-317426ebe7f4","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"11f3c460-eb24-42fa-baca-317426ebe7f4","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"11f3c460-eb24-42fa-baca-317426ebe7f4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dccd0eac-832a-4e1b-824a-65a03e6449fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dccd0eac-832a-4e1b-824a-65a03e6449fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"dccd0eac-832a-4e1b-824a-65a03e6449fc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f773bc0-09aa-45c8-9b18-4f935d61b6fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f773bc0-09aa-45c8-9b18-4f935d61b6fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"4f773bc0-09aa-45c8-9b18-4f935d61b6fc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23d7e327-f6ad-4370-9628-4839d0ab66ba","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23d7e327-f6ad-4370-9628-4839d0ab66ba","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"23d7e327-f6ad-4370-9628-4839d0ab66ba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23ea5733-ba81-4867-9338-089c136125ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23ea5733-ba81-4867-9338-089c136125ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"23ea5733-ba81-4867-9338-089c136125ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53d9bb95-1723-4029-b171-0b835c4cacf9","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53d9bb95-1723-4029-b171-0b835c4cacf9","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"53d9bb95-1723-4029-b171-0b835c4cacf9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c2ccd4a3-12f7-45f2-80f1-7be6f5467262","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c2ccd4a3-12f7-45f2-80f1-7be6f5467262","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"c2ccd4a3-12f7-45f2-80f1-7be6f5467262","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0f2aca60-8e44-45d0-b07d-3afeee7c3252","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0f2aca60-8e44-45d0-b07d-3afeee7c3252","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"0f2aca60-8e44-45d0-b07d-3afeee7c3252","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a2b36f62-ee2a-481e-99d5-85810c5067a0","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a2b36f62-ee2a-481e-99d5-85810c5067a0","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"a2b36f62-ee2a-481e-99d5-85810c5067a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"08b0dd6e-652e-443f-b2f5-23e051dd6aa3","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"08b0dd6e-652e-443f-b2f5-23e051dd6aa3","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"08b0dd6e-652e-443f-b2f5-23e051dd6aa3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0ae3343f-e211-43b1-96a0-c644c7f7336c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0ae3343f-e211-43b1-96a0-c644c7f7336c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"0ae3343f-e211-43b1-96a0-c644c7f7336c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d0c71563-a96c-42c3-bb38-200d05175cfc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d0c71563-a96c-42c3-bb38-200d05175cfc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"d0c71563-a96c-42c3-bb38-200d05175cfc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3bdb5dda-0ece-430c-a7d5-1fd8016e0b32","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3bdb5dda-0ece-430c-a7d5-1fd8016e0b32","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"3bdb5dda-0ece-430c-a7d5-1fd8016e0b32","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"75d4410f-51b6-4dae-ba47-1bbb5e30cd4e","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"75d4410f-51b6-4dae-ba47-1bbb5e30cd4e","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"75d4410f-51b6-4dae-ba47-1bbb5e30cd4e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"091c3427-a9bf-4f6a-a246-8ab0b511620b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"091c3427-a9bf-4f6a-a246-8ab0b511620b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"091c3427-a9bf-4f6a-a246-8ab0b511620b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88f12308-d19c-4eb7-beed-1fcf81ded8d2","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88f12308-d19c-4eb7-beed-1fcf81ded8d2","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"88f12308-d19c-4eb7-beed-1fcf81ded8d2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3671dbc-36a6-491f-93df-157cfc402ed1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3671dbc-36a6-491f-93df-157cfc402ed1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"e3671dbc-36a6-491f-93df-157cfc402ed1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"023bd26a-424d-4c65-8142-9b86ed9b3b42","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"023bd26a-424d-4c65-8142-9b86ed9b3b42","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"023bd26a-424d-4c65-8142-9b86ed9b3b42","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3948a9dd-5b1f-45f8-bb4a-bbda51016dc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3948a9dd-5b1f-45f8-bb4a-bbda51016dc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"3948a9dd-5b1f-45f8-bb4a-bbda51016dc8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8909fad4-a890-4952-8528-477a25c4760a","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8909fad4-a890-4952-8528-477a25c4760a","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"8909fad4-a890-4952-8528-477a25c4760a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6cb99ba1-8a38-4d9e-9ab1-88b099981ae8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6cb99ba1-8a38-4d9e-9ab1-88b099981ae8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"6cb99ba1-8a38-4d9e-9ab1-88b099981ae8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e50a26cc-ce1d-4a6b-8bb2-288f0f65abc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e50a26cc-ce1d-4a6b-8bb2-288f0f65abc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"e50a26cc-ce1d-4a6b-8bb2-288f0f65abc8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cecb5a18-2b36-4057-8724-0e63141e01d5","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cecb5a18-2b36-4057-8724-0e63141e01d5","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"cecb5a18-2b36-4057-8724-0e63141e01d5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"10ec8535-7183-44b9-83f3-1745a9ef574c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"10ec8535-7183-44b9-83f3-1745a9ef574c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"10ec8535-7183-44b9-83f3-1745a9ef574c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c81d434-6d79-4c1c-9a92-c5010e7827be","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c81d434-6d79-4c1c-9a92-c5010e7827be","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"3c81d434-6d79-4c1c-9a92-c5010e7827be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43285300-7337-4b8b-9ce1-21a5e39c7b5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43285300-7337-4b8b-9ce1-21a5e39c7b5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"43285300-7337-4b8b-9ce1-21a5e39c7b5b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"859ed323-6158-4d71-9731-61a5f89b85ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"859ed323-6158-4d71-9731-61a5f89b85ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"859ed323-6158-4d71-9731-61a5f89b85ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7ef8bc36-4de4-403c-8eef-46bbd5f6404f","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7ef8bc36-4de4-403c-8eef-46bbd5f6404f","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"7ef8bc36-4de4-403c-8eef-46bbd5f6404f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c9ecc32e-2559-4227-8a48-06935da8719c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c9ecc32e-2559-4227-8a48-06935da8719c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"LayerId":{"name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","name":"c9ecc32e-2559-4227-8a48-06935da8719c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 30.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ad1bbaf5-26fb-4862-88b2-cd2696beb1a4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dc0c512e-2149-4f6a-8462-32bea150ea5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78661311-32b3-467e-adcc-12b999afffd3","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"11f3c460-eb24-42fa-baca-317426ebe7f4","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"92c709da-c102-43b6-a658-4a487a7f9a79","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dccd0eac-832a-4e1b-824a-65a03e6449fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c75fdc6c-8aa3-41ce-a157-dc85bb6f7701","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f773bc0-09aa-45c8-9b18-4f935d61b6fc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5117371b-28e9-4b89-8997-ee0ae7c80156","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23d7e327-f6ad-4370-9628-4839d0ab66ba","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48a04f3b-efde-49d7-a464-adb9e0a04934","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23ea5733-ba81-4867-9338-089c136125ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f160499f-2ded-499a-90f7-7005ac6dc15a","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53d9bb95-1723-4029-b171-0b835c4cacf9","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9c461d6-d3a0-45d9-9c07-201ea0a859f2","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2ccd4a3-12f7-45f2-80f1-7be6f5467262","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"342ef191-526b-4b49-a476-b2ccd06c6cdb","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0f2aca60-8e44-45d0-b07d-3afeee7c3252","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"119951e3-eff8-4b17-97ab-436365c174b7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2b36f62-ee2a-481e-99d5-85810c5067a0","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32f23760-6046-4f7e-aae4-7c93ff2d6b5a","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"08b0dd6e-652e-443f-b2f5-23e051dd6aa3","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"703c562a-b9ea-4a79-a676-938f6ab84470","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ae3343f-e211-43b1-96a0-c644c7f7336c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95c5cf39-4274-405c-b1e8-3ccd40c8de22","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d0c71563-a96c-42c3-bb38-200d05175cfc","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08fd248a-2ecc-47b8-b5c1-393473878fdc","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3bdb5dda-0ece-430c-a7d5-1fd8016e0b32","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08fa46a5-62e6-4c7f-8111-dd0dd7a589e6","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"75d4410f-51b6-4dae-ba47-1bbb5e30cd4e","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d8a034e-71b6-455d-bf64-1def91a8a08b","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"091c3427-a9bf-4f6a-a246-8ab0b511620b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbf11094-f543-4ec7-9616-3e8ea18c962e","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88f12308-d19c-4eb7-beed-1fcf81ded8d2","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00350800-6fc7-4d87-93a4-95af3cb98556","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3671dbc-36a6-491f-93df-157cfc402ed1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"642b59f5-f262-49c3-9edc-f1db44cb23a5","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"023bd26a-424d-4c65-8142-9b86ed9b3b42","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d037dea-3c49-42d8-97cd-5ed0f4fa9655","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3948a9dd-5b1f-45f8-bb4a-bbda51016dc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c00f5473-c16a-4361-b69b-d9d95ad08838","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8909fad4-a890-4952-8528-477a25c4760a","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"faef6fd0-5c88-4125-8438-6daf15d565fd","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6cb99ba1-8a38-4d9e-9ab1-88b099981ae8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"11bda4fa-2f6c-4157-afe8-e8d91445b0e1","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e50a26cc-ce1d-4a6b-8bb2-288f0f65abc8","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8bf86621-6faf-46a8-bc79-0ef026048ce0","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cecb5a18-2b36-4057-8724-0e63141e01d5","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9177d8f-d289-4875-a221-459494e27edd","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"10ec8535-7183-44b9-83f3-1745a9ef574c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac2900ae-1a1b-4643-8d34-455f724c7e4c","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c81d434-6d79-4c1c-9a92-c5010e7827be","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac7299b6-cf20-4db2-9e87-f3e0a27a0d3c","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43285300-7337-4b8b-9ce1-21a5e39c7b5b","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d69464b4-6ddc-4a34-81d8-6506dbbf9b04","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"859ed323-6158-4d71-9731-61a5f89b85ed","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"470191cd-b465-436e-b7e6-1941c87a00e6","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7ef8bc36-4de4-403c-8eef-46bbd5f6404f","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5150e54-a0cc-4601-b9d7-d2cd53adb54b","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c9ecc32e-2559-4227-8a48-06935da8719c","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 93,
    "yorigin": 105,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Plant_1","path":"sprites/spr_Plant_1/spr_Plant_1.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Plant_1",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"254dc40d-265e-4ed2-ad77-5f03b8f27fcd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Plant_1",
  "tags": [],
  "resourceType": "GMSprite",
}