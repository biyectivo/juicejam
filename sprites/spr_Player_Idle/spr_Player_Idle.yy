{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 52,
  "bbox_right": 75,
  "bbox_top": 52,
  "bbox_bottom": 100,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"af170ea7-db5a-406f-953c-44046d972248","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af170ea7-db5a-406f-953c-44046d972248","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"af170ea7-db5a-406f-953c-44046d972248","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2e8dee8d-e99c-46a2-958f-02f82241a55b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2e8dee8d-e99c-46a2-958f-02f82241a55b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"2e8dee8d-e99c-46a2-958f-02f82241a55b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"27533f2d-9e1f-4899-a907-cc673d37f550","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"27533f2d-9e1f-4899-a907-cc673d37f550","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"27533f2d-9e1f-4899-a907-cc673d37f550","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57b46cdb-af02-45bc-9645-0c5f8ffe82c7","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57b46cdb-af02-45bc-9645-0c5f8ffe82c7","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"57b46cdb-af02-45bc-9645-0c5f8ffe82c7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2e56e418-e1d2-4c33-9506-03ff4d6b23a4","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2e56e418-e1d2-4c33-9506-03ff4d6b23a4","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"2e56e418-e1d2-4c33-9506-03ff4d6b23a4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5cc74358-65a7-477a-88ec-06ac375765fc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5cc74358-65a7-477a-88ec-06ac375765fc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"5cc74358-65a7-477a-88ec-06ac375765fc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8541cd72-8988-4fdc-acf0-f67085795e7b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8541cd72-8988-4fdc-acf0-f67085795e7b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"8541cd72-8988-4fdc-acf0-f67085795e7b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b35a5cf9-2261-4a1d-8c1b-77823b9ab244","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b35a5cf9-2261-4a1d-8c1b-77823b9ab244","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"b35a5cf9-2261-4a1d-8c1b-77823b9ab244","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43f80f3d-8d9c-42e8-8295-049197e87b37","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43f80f3d-8d9c-42e8-8295-049197e87b37","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"43f80f3d-8d9c-42e8-8295-049197e87b37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"acd0d154-6593-4897-906f-0b52ff2fb6f6","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"acd0d154-6593-4897-906f-0b52ff2fb6f6","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"acd0d154-6593-4897-906f-0b52ff2fb6f6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"642b7de3-b927-445a-b524-b7014cb095d1","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"642b7de3-b927-445a-b524-b7014cb095d1","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"642b7de3-b927-445a-b524-b7014cb095d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2993d3d8-1e43-4d27-8f99-b3a4fb3ac998","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2993d3d8-1e43-4d27-8f99-b3a4fb3ac998","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"2993d3d8-1e43-4d27-8f99-b3a4fb3ac998","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"babc8ff7-6bde-4caf-9874-657253d0531b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"babc8ff7-6bde-4caf-9874-657253d0531b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"babc8ff7-6bde-4caf-9874-657253d0531b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"44ef6f95-0623-4996-85b6-f335b14ea231","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"44ef6f95-0623-4996-85b6-f335b14ea231","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"44ef6f95-0623-4996-85b6-f335b14ea231","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f3263c9-d6bd-4095-8e1e-a0bf5c8774ad","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f3263c9-d6bd-4095-8e1e-a0bf5c8774ad","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"3f3263c9-d6bd-4095-8e1e-a0bf5c8774ad","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a1258169-a768-493a-86e1-73f811df12cc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1258169-a768-493a-86e1-73f811df12cc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"a1258169-a768-493a-86e1-73f811df12cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d8772b7d-96b4-4601-a391-1fa3189f81cf","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8772b7d-96b4-4601-a391-1fa3189f81cf","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"d8772b7d-96b4-4601-a391-1fa3189f81cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3dba3c1a-3c8e-4294-b6b3-8798a3692478","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3dba3c1a-3c8e-4294-b6b3-8798a3692478","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"3dba3c1a-3c8e-4294-b6b3-8798a3692478","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"653b159e-ff53-45b4-b6a7-5718d2a330f5","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"653b159e-ff53-45b4-b6a7-5718d2a330f5","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"653b159e-ff53-45b4-b6a7-5718d2a330f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88d18aad-fdb0-455c-95aa-69161ec8fa31","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88d18aad-fdb0-455c-95aa-69161ec8fa31","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"LayerId":{"name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","name":"88d18aad-fdb0-455c-95aa-69161ec8fa31","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 20.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"eed219df-8096-461d-b362-c5c4cac1554d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af170ea7-db5a-406f-953c-44046d972248","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d6e1d431-088d-40d5-beae-50dadb1f46aa","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2e8dee8d-e99c-46a2-958f-02f82241a55b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9e8d773-85ba-4ae6-b26e-781f9e5884e0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"27533f2d-9e1f-4899-a907-cc673d37f550","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b93db75c-eebf-48dc-88df-4dae200b4489","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57b46cdb-af02-45bc-9645-0c5f8ffe82c7","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64aeeaed-9cee-434f-a8d7-e8a180ca2917","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2e56e418-e1d2-4c33-9506-03ff4d6b23a4","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2acbcb01-7b4f-4a5d-a03d-5f983f6f845b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5cc74358-65a7-477a-88ec-06ac375765fc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5729ddc9-2c27-44df-a4e1-fdf7fbdc0ea8","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8541cd72-8988-4fdc-acf0-f67085795e7b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4c2519c3-ec6d-4992-8940-90b31612e323","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b35a5cf9-2261-4a1d-8c1b-77823b9ab244","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3b16c3f2-6065-4f7b-b81e-9a9bae16dde0","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43f80f3d-8d9c-42e8-8295-049197e87b37","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"60c938fb-a949-416e-bb3e-ea289ffec8fd","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"acd0d154-6593-4897-906f-0b52ff2fb6f6","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d712250c-b49b-4e89-b74c-d5b89bbb62e5","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"642b7de3-b927-445a-b524-b7014cb095d1","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc54034d-45bf-4940-98b9-39c11c5fc30f","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2993d3d8-1e43-4d27-8f99-b3a4fb3ac998","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3272095a-0a2a-4f7c-a33a-1ce9a35ac502","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"babc8ff7-6bde-4caf-9874-657253d0531b","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2605f1e7-9e1b-4d1a-907f-0d349dae5ed1","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"44ef6f95-0623-4996-85b6-f335b14ea231","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24e0ab0a-31da-42a5-affb-c329887ba01d","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f3263c9-d6bd-4095-8e1e-a0bf5c8774ad","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7300ddef-07ee-4661-9956-014aed41a4b0","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1258169-a768-493a-86e1-73f811df12cc","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"48b869ad-87bb-468d-a770-45fd9e02bb97","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8772b7d-96b4-4601-a391-1fa3189f81cf","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"84aef5c3-459a-4881-b1b5-8c2c26965b75","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3dba3c1a-3c8e-4294-b6b3-8798a3692478","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aef485de-7dcf-4f27-a31d-277ac85999df","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"653b159e-ff53-45b4-b6a7-5718d2a330f5","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3372b41-81c7-4548-99fa-dafab3646462","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88d18aad-fdb0-455c-95aa-69161ec8fa31","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 100,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Player_Idle","path":"sprites/spr_Player_Idle/spr_Player_Idle.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Player_Idle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4b7be24f-a165-4c23-a3fb-5580d5c79b7a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Animations/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Player_Idle",
  "tags": [],
  "resourceType": "GMSprite",
}