{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 52,
  "bbox_right": 75,
  "bbox_top": 52,
  "bbox_bottom": 100,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "width": 128,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ccbfbe1d-2c8e-49e9-87d9-c020af9ddea3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ccbfbe1d-2c8e-49e9-87d9-c020af9ddea3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"ccbfbe1d-2c8e-49e9-87d9-c020af9ddea3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98125423-aa0b-4ed2-8e8a-2a1109bc6883","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98125423-aa0b-4ed2-8e8a-2a1109bc6883","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"98125423-aa0b-4ed2-8e8a-2a1109bc6883","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a5f15fc2-205d-42dd-807d-6fee177d89d3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5f15fc2-205d-42dd-807d-6fee177d89d3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"a5f15fc2-205d-42dd-807d-6fee177d89d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06c17e8f-1c16-4af0-8e56-f4cab2c2f739","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06c17e8f-1c16-4af0-8e56-f4cab2c2f739","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"06c17e8f-1c16-4af0-8e56-f4cab2c2f739","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77992d18-90f7-4815-976e-e937bdf5215e","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77992d18-90f7-4815-976e-e937bdf5215e","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"77992d18-90f7-4815-976e-e937bdf5215e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a1a03c38-4915-4622-b2f5-1f0402efa9a1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1a03c38-4915-4622-b2f5-1f0402efa9a1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"a1a03c38-4915-4622-b2f5-1f0402efa9a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c7202810-097c-4b44-922b-1be20baab925","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7202810-097c-4b44-922b-1be20baab925","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"c7202810-097c-4b44-922b-1be20baab925","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e444db7a-b2d4-41c2-a5f8-bd3a0f9ce250","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e444db7a-b2d4-41c2-a5f8-bd3a0f9ce250","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"LayerId":{"name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","name":"e444db7a-b2d4-41c2-a5f8-bd3a0f9ce250","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9cda8e09-d4b3-4d00-b6af-891fce034aec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ccbfbe1d-2c8e-49e9-87d9-c020af9ddea3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"108b8abc-d7ce-4b48-acbc-83f8e6586a0e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98125423-aa0b-4ed2-8e8a-2a1109bc6883","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6ab5e03-b975-4bd8-bc53-1a6b640fa96e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5f15fc2-205d-42dd-807d-6fee177d89d3","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d169d1c-a298-4cd7-9a24-d69df135a385","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06c17e8f-1c16-4af0-8e56-f4cab2c2f739","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"23a7d7dc-8194-43e0-bf5f-45a0669330b0","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77992d18-90f7-4815-976e-e937bdf5215e","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a59d7982-dae5-4a3c-b252-d54b65a0beee","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1a03c38-4915-4622-b2f5-1f0402efa9a1","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06c70055-b621-48bb-8972-c43950133cae","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7202810-097c-4b44-922b-1be20baab925","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fdd6e616-c3fe-4c11-afae-bd0200b3b5de","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e444db7a-b2d4-41c2-a5f8-bd3a0f9ce250","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 100,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Player_Jump","path":"sprites/spr_Player_Jump/spr_Player_Jump.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Player_Jump",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d04f3932-e692-43e1-bd59-ae5b526aeaf1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Animations/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Player_Jump",
  "tags": [],
  "resourceType": "GMSprite",
}