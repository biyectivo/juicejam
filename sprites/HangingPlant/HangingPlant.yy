{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 120,
  "bbox_top": 2,
  "bbox_bottom": 345,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 125,
  "height": 353,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d0dfcc09-3d2c-413a-8b75-dd8564a8dd5b","path":"sprites/HangingPlant/HangingPlant.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d0dfcc09-3d2c-413a-8b75-dd8564a8dd5b","path":"sprites/HangingPlant/HangingPlant.yy",},"LayerId":{"name":"a60ad7a9-cca2-42d2-a417-5390e0b7aa3f","path":"sprites/HangingPlant/HangingPlant.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"HangingPlant","path":"sprites/HangingPlant/HangingPlant.yy",},"resourceVersion":"1.0","name":"d0dfcc09-3d2c-413a-8b75-dd8564a8dd5b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"HangingPlant","path":"sprites/HangingPlant/HangingPlant.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"98f6505e-833e-43da-9278-47b7226c53d5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d0dfcc09-3d2c-413a-8b75-dd8564a8dd5b","path":"sprites/HangingPlant/HangingPlant.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 62,
    "yorigin": 176,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"HangingPlant","path":"sprites/HangingPlant/HangingPlant.yy",},
    "resourceVersion": "1.3",
    "name": "HangingPlant",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a60ad7a9-cca2-42d2-a417-5390e0b7aa3f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "HangingPlant",
  "tags": [],
  "resourceType": "GMSprite",
}