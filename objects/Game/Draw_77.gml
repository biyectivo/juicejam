/// @description Draw Game
/*
// Disable alpha blending 
gpu_set_blendenable(false);

// Draw app surface
if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW) {
	if (window_get_fullscreen()) {
		Game.gui_offset_x = (DISPLAY_WIDTH - adjusted_resolution_width)/2;
		Game.gui_offset_y = (DISPLAY_HEIGHT - adjusted_resolution_height)/2;
		Game.application_surface_scaling = 1;
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
	}
	else {
		Game.gui_offset_x = (adjusted_window_width - adjusted_resolution_width)/2;
		Game.gui_offset_y = (adjusted_window_height - adjusted_resolution_height)/2;	
		Game.application_surface_scaling = 1;
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
	}
	//draw_surface_ext(application_surface, Game.gui_offset_x, Game.gui_offset_y, Game.application_surface_scaling, Game.application_surface_scaling, 0, c_white, 1);	
	draw_surface_ext(application_surface, Game.gui_offset_x, Game.gui_offset_y, _factorw, _factorh, 0, c_white, 1);	

	display_set_gui_size(adjusted_window_width, adjusted_window_height);
	display_set_gui_maximize(1, 1, 0, 0);
	
}
else if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_SCALED) {
	if (window_get_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;	
		if (RESOLUTION_MAXIMIZED_SCALING_FACTOR == -1) {
			Game.application_surface_scaling = min(_factorw, _factorh);
		}
		else {
			Game.application_surface_scaling = min(RESOLUTION_MAXIMIZED_SCALING_FACTOR, _factorw, _factorh);
		}
		Game.gui_offset_x = (DISPLAY_WIDTH - adjusted_resolution_width*Game.application_surface_scaling)/2;
		Game.gui_offset_y = (DISPLAY_HEIGHT - adjusted_resolution_height*Game.application_surface_scaling)/2;		
	}
	else {
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
		if (RESOLUTION_MAXIMIZED_SCALING_FACTOR == -1) {
			Game.application_surface_scaling = min(_factorw, _factorh);
		}
		else {
			Game.application_surface_scaling = min(RESOLUTION_MAXIMIZED_SCALING_FACTOR, _factorw, _factorh);
		}
		Game.gui_offset_x = (adjusted_window_width - adjusted_resolution_width*Game.application_surface_scaling)/2;
		Game.gui_offset_y = (adjusted_window_height - adjusted_resolution_height*Game.application_surface_scaling)/2;		
	}
	
	draw_surface_ext(application_surface, Game.gui_offset_x, Game.gui_offset_y, Game.application_surface_scaling, Game.application_surface_scaling, 0, c_white, 1);		
	
	// GUI layer - workaround!!! TODO: fix this
	display_set_gui_maximize();	
	
}
else {
	Game.gui_offset_x = 0;
	Game.gui_offset_y = 0;
	if (window_get_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
		Game.application_surface_scaling = min(_factorw, _factorh);		
	}
	else {
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
		Game.application_surface_scaling = min(_factorw, _factorh);		
	}
	display_set_gui_size(adjusted_window_width, adjusted_window_height);
	display_set_gui_maximize();
}

// Re-enable alpha blending 
gpu_set_blendenable(true);
*/



