/// @description Debug functions

if (Game.debug) {
	// Room restart
	if (keyboard_check_pressed(vk_f2)) {
		room_restart();	
	}

	// Game restart
	if (keyboard_check_pressed(vk_f3)) {
		game_restart();
	}
}

// Toggle debug mode
if (keyboard_check_pressed(vk_f1)) {
	debug = !debug;
	layer_set_visible("lyr_Tile_Collision", debug);
}

