// Enable views and set up graphics
view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	

// Particle system
particle_system = part_system_create_layer("lyr_Particles", true);
particle_system_behind = part_system_create_layer("lyr_Particles_Behind", true);
	

if (room == room_Game_1 || room == room_Game_Tutorial) {
	
	// Set room
	if (room == room_Game_1) {
		room_height = WORLD_HEIGHT_TILES * TILE_SIZE;
	}
	
	if (instance_exists(obj_LavaArea)) {
		obj_LavaArea.y = room_height;	
	}
	
	//*****************************************************************************
	// Randomize
	//*****************************************************************************

	seed = randomize();
	//seed = 3956990956;
	//seed = 713040478;
	//seed = 2737019624;
	//seed = 1506754822;
	//seed = 4288800760;
	//seed = 272374246;
	// seed = 3846302351; // impossible level
	// seed = 1069758349; // impossible level
	// seed = 1877173822; // impossible level
	//seed = 1614379261;
	//seed = 253064770;
	//seed = 1359587785;
	//seed = 3577535842;
	//seed = 1101879659;
	//seed = 1403149281;
							//seed = 1511477846;
	//seed = 1563182444;
	//seed = 407769102;
	//seed = 4261934791;
	//seed = 1809081237;
	//seed = 36965363;
	//seed = 286988633;
	//seed = 1894148904;
	//seed = 2846990795; // impossible level
	//seed = 1941230;
	//seed = 3660108475;
	//seed = 198116186; //**
	//seed = 2501938397; // level with an impossible
	//seed = 1189236668; // Impossible
	//seed = 2092458961;
	random_set_seed(seed);
	print("Seed: "+str(seed));
	//clipboard_set_text(str(seed));
	
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Generate level
	if (room == room_Game_1) {
		world = ds_grid_create(room_width/TILE_SIZE, WORLD_HEIGHT_TILES);
		world_background = ds_grid_create(room_width/TILE_SIZE, WORLD_HEIGHT_TILES);
		fnc_GenerateLevel();
		show_debug_message(str(room_width/TILE_SIZE)+" cols and "+str(WORLD_HEIGHT_TILES)+" rows");
		
		started = true;
		// Play music	
		// https://www.beepbox.co/#9n31sbk0l04e09t2ma7g0vj07r1i0o324T5v2u05f62ge2ec2f02j01960meq83432d38HT-Iqijriiiih99h0E0T5v0u50f0qwx10p511d08H-JJAArrqiih999h0E1b6T5v1ua6f10i8q8141d23HYr901i8ah00000h0E0T4v1uf0f0q011z6666ji8k8k3jSBKSJJAArriiiiii07JCABrzrrrrrrr00YrkqHrsrrrrjr005zrAqzrjzrrqr1jRjrqGGrrzsrsA099ijrABJJJIAzrrtirqrqjqixzsrAjrqjiqaqqysttAJqjikikrizrHtBJJAzArzrIsRCITKSS099ijrAJS____Qg99habbCAYrDzh00E0b4h4i4h4Q4g0000000000014h8Qh4lAg000000000000h4h4y8ycy0000000000000000014h4g000000000000p22pFKDNQMSrF-_j6hWJmVjtf-A0kQu88W2ewzE8W2ewzE8W938yewzE8W2eAzE8X2eAzFHY9hhhhhBlkQYmkkkil5560knQVt555t550IR_2gLFALFALwkO-CLOtv8O4z8O4y2Cze4tp7ihQAt97ihQAt960
		if (option_value[? "Music"]) {
			music_sound_id = audio_play_sound(snd_Music, 3, true);
		}
	
	}
	total_possible_score = instance_number(obj_BlueFlower);
	camera_lerp = false;
	
	part_system_automatic_draw(Game.particle_system, true);
	
	
}
