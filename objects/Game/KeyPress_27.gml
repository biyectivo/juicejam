/// @description Pause

	if (Game.started && !Game.lost && !Game.won) {			
		if (room == room_Game_1) {
			paused = !paused;		
			if (paused) {
				screen_save_part("_tmp_screenshot.png", 0, 0, window_get_width(), window_get_height());
				pause_screenshot = sprite_add("_tmp_screenshot.png", 0, false, false, 0, 0);
				instance_deactivate_all(true);
			}
			else {
				instance_activate_all();
			}
		}
		else {
			room_goto(room_Game_Tutorial);
		}
	}	
