//*****************************************************************************
// Handle camera
//*****************************************************************************


if (!Game.paused) {	
	
	if (room == room_Game_1 || room == room_Game_Tutorial) {
		
		if (instance_exists(camera_target)) {
			
			// Get current camera position
			var _current_camera_x = camera_get_view_x(VIEW);
			var _current_camera_y = camera_get_view_y(VIEW);
	
			// Calculate offset for screen shake			
			var _offsetx = 0;
			var _offsety = 0;
			if (camera_shake) {
				var _offsetx = irandom_range(-screenshake_strength,screenshake_strength);
				var _offsety = irandom_range(-screenshake_strength,screenshake_strength);
			}
			
			if (room == room_Game_1) {
				// Get target camera position, centered around the target and lerped with camera smoothness
				var _destx = clamp(camera_target.x-adjusted_resolution_width/2, 0, room_width-adjusted_resolution_width);
				//var _desty = clamp(camera_target.y-adjusted_resolution_height/2, 0, room_height-adjusted_resolution_height);
				var _desty = min(camera_target.y-adjusted_resolution_height/2, room_height-adjusted_resolution_height);
			}
			else {
				var _destx = 0;
				var _desty = clamp(camera_target.y-adjusted_resolution_height, 0, room_height-adjusted_resolution_height);
			}

			if (camera_lerp) {
				var _target_camera_x = lerp(_current_camera_x, _destx, camera_smoothness) + _offsetx;
				var _target_camera_y = lerp(_current_camera_y, _desty, camera_smoothness) + _offsety;
			}
			else {
				var _target_camera_x = _destx;
				var _target_camera_y = _desty;
				camera_lerp = true;
			}
			// Move camera
			camera_set_view_pos(VIEW, _target_camera_x, _target_camera_y);
			
				
		}
		
	}
	
}

//*****************************************************************************
// Detect primary gamepad
//*****************************************************************************

var _maxpads = gamepad_get_device_count();
Game.primary_gamepad = -1;
var _i=0; 
while (_i < _maxpads && Game.primary_gamepad == -1) {
	Game.primary_gamepad = gamepad_is_connected(_i) ? _i : -1;
	_i++;
}