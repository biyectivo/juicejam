
fnc_BackupDrawParams();

//switch (room) {
	/*
	case room_UI_Title:
		fnc_DrawMenu();
		break;
	case room_UI_Options:
		fnc_DrawOptions();
		break;
	case room_UI_Options_Controls:
		fnc_DrawOptionsControls();
		break;
	case room_UI_Credits:
		fnc_DrawCredits();		
		break;
	case room_UI_HowToPlay:
		fnc_DrawHowToPlay();
		break;
	*/
	if (started) {
		if (Game.paused) {
			if (Game.lost || Game.won) {
				instance_deactivate_all(true);
				if (instance_exists(obj_gmlive)) instance_activate_object(obj_gmlive);
				fnc_DrawYouLost();
			}
			else {				
				fnc_DrawPauseMenu();
			}
		}
		else {
			fnc_DrawHUD();
		}
	}
	else {
		fnc_DrawMenu();	
	}
		//break;
//}

// Draw debug overlay on top of everything else
if (debug) {
	fnc_DrawDebug();
}

fnc_RestoreDrawParams();
