if (keyboard_check_pressed(ord("M"))) {
	option_value[? "Sounds"] = !option_value[? "Sounds"];
	option_value[? "Music"] = !option_value[? "Music"];
	
	if (!option_value[? "Music"] && audio_is_playing(music_sound_id)) {
		audio_pause_sound(music_sound_id);	
	}
	else if (option_value[? "Music"] && audio_is_paused(music_sound_id)) {
		audio_resume_sound(music_sound_id);	
	}
	
	if (!option_value[? "Sounds"] && audio_is_playing(obj_LavaArea.volcano_sound)) {
		audio_pause_sound(obj_LavaArea.volcano_sound)
	}
	else if (option_value[? "Sounds"] && audio_is_paused(obj_LavaArea.volcano_sound)) {
		audio_resume_sound(obj_LavaArea.volcano_sound)
	}
}

if ((room == room_Game_1 || room == room_Game_Tutorial) && !Game.won && !Game.lost) {
	total_time++;
	if (current_step % 10 == 0) {
		var _x = irandom_range(camera_get_view_x(VIEW), camera_get_view_x(VIEW)+camera_get_view_width(VIEW));
		var _y = irandom_range(camera_get_view_y(VIEW), camera_get_view_y(VIEW)+camera_get_view_height(VIEW));
		part_particles_create(Game.particle_system_behind, _x, _y, part_ember, 1);
	}
	// Global step
	if (instance_exists(obj_Player)) {
		current_step++;
	}					
}

if (!Game.lost && !Game.won && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if ((Game.lost || Game.won) && Game.paused) {
	if ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter))) {
		room_restart();
	}
	if ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_select)) || (keyboard_check_pressed(vk_escape))) {
		room_goto(room_Game_Tutorial);
	}
}



// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}
