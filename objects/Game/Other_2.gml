// Mouse cursor
//window_set_cursor(cr_none);
//cursor_sprite = spr_Cursor;

// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
if (SELECTED_SCALING != SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW) {
	//application_surface_draw_enable(false);
}


// Anti-aliasing and vsync
//display_reset(8, false);

// Go to next room
//room_goto(room_UI_Title);
room_goto(room_Game_Tutorial);
//room_goto(room_Game_1);

