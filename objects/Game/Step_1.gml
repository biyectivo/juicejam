//  Handle fullscreen change
if (!BROWSER && keyboard_check(vk_lalt) && keyboard_check_pressed(vk_enter)) {
	fnc_SetGraphics();
}

// Browser "fullscreen" hack
if (BROWSER && keyboard_check_pressed(ord("F"))) {
	option_value[? "Fullscreen"] = !option_value[? "Fullscreen"];
	if (option_value[? "Fullscreen"]) fnc_SetGraphics();
				
	//ToggleFullScreen();
}

/*if (keyboard_check_pressed(ord("Z"))) {
	mult = (mult + 1) % 3;
	fnc_SetGraphics();
	camera_lerp = false;
}*/

// Fullscreen has changed (either via ALT+TAB or F10 on HTML5 or via menu)
if (fullscreen_change) {	
	window_set_fullscreen(option_value[? "Fullscreen"]);
	fullscreen_change = false;	
	// Update graphics
	fnc_SetGraphics();	
}


if (Game.primary_gamepad != -1 && gamepad_button_check_pressed(Game.primary_gamepad, gp_select)) {
	game_restart();	
}