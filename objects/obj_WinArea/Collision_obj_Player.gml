with (other) {
	if (alarm[1] <= 0) {		
		visible = false;
		alarm[1] = 180;
		repeat(100) {
			var _x = irandom_range(0, room_width);
			var _y = irandom_range(-5*TILE_SIZE, 0);
			part_particles_create(Game.particle_system, _x, _y, part_big_star, 1);
		}
	}
	y = -2*TILE_SIZE
}
