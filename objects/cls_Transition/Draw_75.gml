fnc_BackupDrawParams();
if (room != destination_room) {
	screen_save_part("_tmp_screenshot_1.png", 0, 0, window_get_width(), window_get_height());
	view_picture = sprite_add("_tmp_screenshot_1.png", 0, false, false, 0, 0);

	if (room_exists(destination_room) && room != destination_room) {
		room_goto(destination_room);
	}
}
else {
	var _channel = animcurve_get_channel(CSSTransitions, "ease-out");
	if (transition_type == TRANSITION.FADE_OUT) {		
		var _alpha = 1-animcurve_channel_evaluate(_channel, 1-alarm[0]/max_time);
			
		draw_sprite_ext(view_picture, 0, 0, 0, 1, 1, 0, c_white, _alpha);
	}
	else if (transition_type == TRANSITION.STRIPES_HORIZONTAL) {
		var _alpha = 1-animcurve_channel_evaluate(_channel, 1-alarm[0]/max_time);
			
		var _w = sprite_get_width(view_picture);
		var _h = sprite_get_height(view_picture);
		var _num_stripes = 200;
		
		var _stripe_height = _h/_num_stripes;
		for (var _i=0; _i<_num_stripes; _i++) {
			var _x = floor(power(-1, _i))*(max_time-alarm[0])*(_w/max_time);
			draw_sprite_part_ext(view_picture, 0, 0, _i*_stripe_height, _w, _stripe_height, _x, _i*_stripe_height, 1, 1, c_white, _alpha);
		}
		
		//draw_sprite_ext(view_picture, 0, 0, 0, 1, 1, _alpha, c_white, _alpha);
	}
	else if (transition_type == TRANSITION.STRIPES_VERTICAL) {
		var _alpha = 1-animcurve_channel_evaluate(_channel, 1-alarm[0]/max_time);
			
		var _w = sprite_get_width(view_picture);
		var _h = sprite_get_height(view_picture);
		var _num_stripes = 200;
		
		var _stripe_width = _w/_num_stripes;
		for (var _j=0; _j<_num_stripes; _j++) {
			var _y = floor(power(-1, _j))*(max_time-alarm[0])*(_h/max_time);
			draw_sprite_part_ext(view_picture, 0, _j*_stripe_width, 0, _stripe_width, _h, _j*_stripe_width, _y, 1, 1, c_white, _alpha);
		}
		
		//draw_sprite_ext(view_picture, 0, 0, 0, 1, 1, _alpha, c_white, _alpha);
	}
	else if (transition_type == TRANSITION.SQUARES) {		
			
		var _w = sprite_get_width(view_picture);
		var _h = sprite_get_height(view_picture);
		
		var _num_rows = 20;
		var _num_cols = 40;
		
		var _square_width = _w/_num_cols;
		var _square_height = _h/_num_rows;
		
		for (var _i=0; _i<_num_rows; _i++) {
			for (var _j=0; _j<_num_cols; _j++) {
				var _x = _j * _square_width;
				var _y = _i * _square_height;
				draw_sprite_part_ext(view_picture, 0, _j*_square_width, _i*_square_height, _square_width, _square_height, _x, _y, 1, 1, c_white, (_i+_j)/(_num_rows*_num_cols)*max_time * alarm[0]/max_time );
			}
		}
		
		//draw_sprite_ext(view_picture, 0, 0, 0, 1, 1, _alpha, c_white, _alpha);
	}
}
fnc_RestoreDrawParams();
