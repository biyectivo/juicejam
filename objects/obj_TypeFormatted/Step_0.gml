// Process typewriter
var _n = ds_map_size(global.TypeFormatted_parsedElementMap);
var _key = ds_map_find_first(global.TypeFormatted_parsedElementMap);

for (var _i=0; _i<_n; _i++) {
	var _struct = global.TypeFormatted_parsedElementMap[? _key];	
	
	// Process sprite alarms
	var _m = array_length(_struct.current_alarm);
	
	for (var _j=0; _j<_m; _j++) {
		if (_struct.current_alarm[_j] != -1) {
			_struct.current_alarm[_j]--; // Decrement
			if (_struct.current_alarm[_j] == 0) { // Process animation change
				_struct.incrementFrame(_j);
			}
		}
	}
	var _key = ds_map_find_next(global.TypeFormatted_parsedElementMap, _key)
	
	// Process typewriter
	if (_struct.typewriter_delay != -1 && _struct.current_character < _struct.totalCharacters()) {
		if (!_struct.typewriter_paused) {
			if (_struct.typewriter_alarm != -1) {
				_struct.typewriter_alarm--;
				if (_struct.typewriter_alarm == 0) {
					_struct.incrementCharacter();
					// Process action
					_struct.typewriter_action();
				}
			}
		}
	}
	
	// Process blink
	if (!_struct.blink_stopped) {
		_struct.blink_alarm = _struct.blink_alarm + _struct.blink_direction;
		if (_struct.blink_alarm == -abs(_struct.blink_speed)) {
			_struct.blink_direction = 1;
		}
		else if (_struct.blink_alarm == abs(_struct.blink_speed)) {
			_struct.blink_direction = -1;
		}
	}
}

// If autoflush, flush
if (TYPE_FORMATTED_AUTOFLUSH && _n > TYPE_FORMATTED_CACHE_SIZE_FLUSH) {
	fnc_TypeFormatted_Flush();
}
