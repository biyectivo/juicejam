if (sprite_index == spr_BlueFlower_Closed && place_meeting(x, y, obj_Player)) {	
	sprite_index = spr_BlueFlower_Open;		
	Game.total_score++;
	if (Game.option_value[? "Sounds"]) {
		doublejump_sound = audio_play_sound(snd_Flower, 1, false);		
	}
}

if (sprite_index == spr_BlueFlower_Open) {
	repeat(3) {
		part_particles_create(Game.particle_system, x-image_xscale*52-irandom_range(-4,4), y-59-irandom_range(-4,4), part_flower_glow, 1);
	}
}

with (light) {
	x = other.x - other.image_xscale * 52;
	y = other.y - 59;
	xscale = other.sprite_index == spr_BlueFlower_Open ? 0.8 : 0.5;
	yscale = other.sprite_index == spr_BlueFlower_Open ? 0.8 : 0.5;
	visible = true;
}
