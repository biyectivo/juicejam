image_xscale = orientation;
sprite_index = sprite;
image_index = irandom_range(0, sprite_get_number(sprite_index));

light = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_point, 0, x, y);

with (light) {
	blend = $666666;
	xscale = 0.5;
	yscale = 0.5;
	x = other.x - other.orientation * 52;
	y = other.y - 59;
	visible = other.sprite_index == spr_BlueFlower_Open;
}


part_flower_glow = part_type_create();
part_type_size(part_flower_glow, 0.3, 0.6, 0, 0);
part_type_life(part_flower_glow, 20, 40);
part_type_shape(part_flower_glow, pt_shape_spark);
part_type_scale(part_flower_glow, 0.4, 0.4);
part_type_color_mix(part_flower_glow, c_white, c_purple);
part_type_blend(part_flower_glow, true);
part_type_alpha2(part_flower_glow, 1, 0);
part_type_speed(part_flower_glow, 0.05,0.09, 0.05,0);
part_type_direction(part_flower_glow, 0, 359, 0, 20);

