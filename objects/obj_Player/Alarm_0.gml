/// @description Lost - Animation end
Game.lost = true;
Game.paused = true;
part_system_automatic_draw(Game.particle_system, false);
part_particles_clear(Game.particle_system);
part_particles_clear(Game.particle_system_behind);