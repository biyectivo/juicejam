if (ENABLE_LIVE && live_call()) return live_result;

if (instance_exists(obj_LavaArea) && obj_LavaArea.y <= bbox_top) {	
	// Make the distortion smooth instead of pixely
	gpu_set_tex_filter(true);
	// Repeat the texture while the shader moves the coordinates outside (0,1)
	gpu_set_texrepeat(true);

	shader_set(shd_Water);
	var _displacement_sampler = shader_get_sampler_index(shd_Water, "displacement_map");
	var _time_uniform = shader_get_uniform(shd_Water, "time");
	var _strength_uniform = shader_get_uniform(shd_Water, "strength");
	texture_set_stage(_displacement_sampler, sprite_get_texture(spr_Noise,0));
	shader_set_uniform_f(_time_uniform, current_time/1000);
	shader_set_uniform_f(_strength_uniform, 6.0);
}

fnc_BackupDrawParams();
draw_self();
if (Game.debug) {
	draw_set_alpha(0.3);
	draw_set_color(c_red);
	draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
	draw_set_alpha(1);
	type_formatted(x, y-100, "[c_white][fa_center][scale,1]"+fsm.current_state_name+" "+string_format(horizontal_speed, 2, 3)+" "+string_format(vertical_speed, 2, 3)+" gnd="+str(fnc_OnGround())+" jmp="+str(curr_max_jumps));
}

if (instance_exists(obj_LavaArea) && obj_LavaArea.y <= bbox_top) {
	shader_reset();
}

fnc_RestoreDrawParams();