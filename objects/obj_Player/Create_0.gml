/// @description Create player
particle_emitter = part_emitter_create(Game.particle_system);
//part_emitter_region(particle_system, particle_emitter, x-32, x+32, y-5, y+5, ps_shape_ellipse, ps_distr_gaussian);

part_small_star = part_type_create();
part_type_size(part_small_star, 0.4, 0.8, 0, 0);
part_type_life(part_small_star, 10, 25);
part_type_shape(part_small_star, pt_shape_star);
part_type_scale(part_small_star, 0.3, 0.3);
part_type_color_mix(part_small_star, c_purple, c_yellow);
part_type_blend(part_small_star, true);
part_type_alpha2(part_small_star, 1, 0);
part_type_speed(part_small_star, 1,2,0.1,false);
part_type_direction(part_small_star, 0, 359, 0, 20);


part_big_star = part_type_create();
part_type_size(part_big_star, 1, 2, 0, 0);
part_type_life(part_big_star, 20, 40);
part_type_shape(part_big_star, pt_shape_star);
part_type_scale(part_big_star, 0.4, 0.4);
part_type_color_mix(part_big_star, c_purple, c_yellow);
part_type_blend(part_big_star, true);
part_type_alpha2(part_big_star, 1, 0);
part_type_speed(part_big_star, 1,2,0.1,false);
part_type_direction(part_big_star, 0, 359, 0, 20);

part_player_fragment = part_type_create();
part_type_size(part_player_fragment, 3, 10, 0, 0);
part_type_life(part_player_fragment, 30, 60);
part_type_shape(part_player_fragment, pt_shape_sphere);
part_type_scale(part_player_fragment, 0.4, 0.4);
part_type_color_mix(part_player_fragment, c_purple, c_yellow);
part_type_blend(part_player_fragment, true);
part_type_alpha2(part_player_fragment, 1, 0);
part_type_speed(part_player_fragment, 3,8,0.1,false);
part_type_direction(part_player_fragment, 45, 135, 0, 20);


#region Stats
	horizontal_input = 0; 
	vertical_input = 0; 
	superjump_input = 0; 
	
	max_hp = 3;
	hp = max_hp;
	
	superjump_cooldown = 60*5;
	
	horizontal_speed = 0;
	vertical_speed = 0;
	
	//jump_speed = 20;
	jump_speed = 25;
	
	max_horizontal_speed = 16;
	max_vertical_speed = 32;
	
	horizontal_acceleration = 0.02;
	horizontal_deceleration = 0.18;
	horizontal_air_acceleration = horizontal_acceleration*0.8;
	horizontal_air_deceleration = horizontal_deceleration*1.1;
	
	gravity_acceleration = 1.2;

	max_jumps = 2;
	curr_max_jumps = 0;
	
	step = 0;
#endregion

#region Finite State Machine

	#region Helper functions

		fnc_OnGround = function() {
			var _increment_precision = 1;
			var _collision_pos = (vertical_speed >= 0 ? bbox_bottom : bbox_top);
			var _collision_tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
			var _tile_collision = tilemap_get_at_pixel(_collision_tilemap, x, _collision_pos + _increment_precision) == 1;
			return _tile_collision;
		}

		fnc_GetInputOrSensors = function() {
			if (Game.started) {
				horizontal_input = keyboard_check(Game.controls[? "right"]) - keyboard_check(Game.controls[? "left"]);
				if (Game.primary_gamepad != -1) {
					horizontal_input = (gamepad_axis_value(Game.primary_gamepad, gp_axislh) > GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "right"])) - (gamepad_axis_value(Game.primary_gamepad, gp_axislh) < -GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "left"]));
				}
	
				vertical_input = keyboard_check_pressed(Game.controls[? "up"]);
				if (Game.primary_gamepad != -1) {
					vertical_input = gamepad_button_check_pressed(Game.primary_gamepad, gp_face1) || keyboard_check_pressed(Game.controls[? "up"])
				}
			
				superjump_input = keyboard_check_pressed(Game.controls[? "down"]);
				if (Game.primary_gamepad != -1) {
					superjump_input = gamepad_button_check_pressed(Game.primary_gamepad, gp_face2) || keyboard_check_pressed(Game.controls[? "down"])
				}
			}
		}

		fnc_ProcessHorizontalMove = function() {
			var _increment_precision = sign(horizontal_speed);
			//var _increment_precision = 0.01;
			
			// Check for collisions with collisionable objects
			if (place_meeting(x + horizontal_speed, y, cls_Collisionable)) {		
				var _id = instance_place(x + horizontal_speed, y, cls_Collisionable);		
				switch (_id.object_index) {
					case obj_Player:
						x = x + horizontal_speed;
						break;
					default: // Pixel-perfect snap to bounding box
						while (!place_meeting(x + _increment_precision, y, cls_Collisionable)) {
							x = x + _increment_precision;
						}
						horizontal_speed = 0;
						break;
				}
			}
			else { // Check for tile collisions
				var _collision_pos = (horizontal_speed >= 0 ? bbox_right : bbox_left);
				var _collision_tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
				var _tile_collision = tilemap_get_at_pixel(_collision_tilemap, _collision_pos + horizontal_speed, y) == 1;
				if (_tile_collision) { // Pixel-perfect tile collision
					var _tile_collision_per_pixel = tilemap_get_at_pixel(_collision_tilemap, _collision_pos + _increment_precision, y) == 1;
					while (!_tile_collision_per_pixel) {
						x = x + _increment_precision;
						var _collision_pos = (horizontal_speed >= 0 ? bbox_right : bbox_left);
						var _tile_collision_per_pixel = tilemap_get_at_pixel(_collision_tilemap, _collision_pos + _increment_precision, y) == 1;
					}
					horizontal_speed = 0;
				}
				else {
					x = x + horizontal_speed;
				}
			}
			
			//x = floor(x);
		}
	
		fnc_ProcessVerticalMove = function() {
			var _increment_precision = sign(vertical_speed);
			//var _increment_precision = 0.01;
			
			// Check for collisions with collisionable objects
			if (place_meeting(x, y + vertical_speed, cls_Collisionable)) {		
				var _id = instance_place(x, y + vertical_speed, cls_Collisionable);		
				switch (_id.object_index) {
					case obj_Player:
						y = y + vertical_speed;
						break;
					default: // Pixel-perfect snap to bounding box
						while (!place_meeting(x, y + _increment_precision, cls_Collisionable)) {
							y = y + _increment_precision;
						}
						vertical_speed = 0;
						break;
				}
			}
			else { // Check for tile collisions
				var _collision_pos = (vertical_speed >= 0 ? bbox_bottom : bbox_top);
				var _collision_tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
				var _tile_collision = tilemap_get_at_pixel(_collision_tilemap, x, _collision_pos + vertical_speed) == 1;
				if (_tile_collision) { // Pixel-perfect tile collision
					var _tile_collision_per_pixel = tilemap_get_at_pixel(_collision_tilemap, x, _collision_pos + _increment_precision) == 1;
					while (!_tile_collision_per_pixel) {
						y = y + _increment_precision;
						var _collision_pos = (vertical_speed >= 0 ? bbox_bottom : bbox_top);
						var _tile_collision_per_pixel = tilemap_get_at_pixel(_collision_tilemap, x, _collision_pos + _increment_precision) == 1;
					}
					vertical_speed = 0;
				}
				else {
					y = y + vertical_speed;
				}		
			}
			
			//y = floor(y);
		}
		
		fnc_FSM_ResetStateAnimation = function() {
			var _obj = string_copy(object_get_name(object_index), 5, string_length(object_get_name(object_index))-4);
			sprite_index = asset_get_index("spr_"+_obj+"_"+fsm.current_state_name);
			image_index = 0;
			step = 0;			
		}

		fnc_FSM_Animate = function() {
			var _length = sprite_get_number(sprite_index);
			var _speed = sprite_get_speed(sprite_index)
			if (step % _speed == 0) {
				image_index = image_index + 1;
				if (image_index == _length) {
					image_index = 0;
				}
			}
			if (horizontal_speed != 0) {
				image_xscale = sign(horizontal_speed);
			}
		}
	#endregion

	
	#region States
	
		#region Idle
			fsm = new StateMachine();
			var _state = new State("Idle");
			with (_state) {
				enter = function() {
					with (other) {
						//starting_speed = horizontal_speed;
						fnc_FSM_ResetStateAnimation();
					}
				}
				step = function() {
					with (other) {
						//horizontal_speed = clamp(horizontal_speed - sign(horizontal_speed) * horizontal_deceleration, -max_horizontal_speed, max_horizontal_speed);
						if (fnc_OnGround()) {
							curr_max_jumps = 0;
							horizontal_speed = lerp(horizontal_speed, 0, horizontal_deceleration);
							//horizontal_speed = lerp(starting_speed, 0, horizontal_deceleration);
						}
						else {
							horizontal_speed = lerp(horizontal_speed, 0, horizontal_air_deceleration);
							//horizontal_speed = lerp(starting_speed, 0, horizontal_air_deceleration);
						}
						fnc_ProcessHorizontalMove();
				
						//vertical_speed = clamp(vertical_speed - sign(vertical_speed) * vertical_deceleration, -max_vertical_speed, max_vertical_speed);
						vertical_speed = clamp(vertical_speed + gravity_acceleration, -jump_speed, max_vertical_speed);
						fnc_ProcessVerticalMove();
				
						fnc_FSM_Animate();
						fnc_GetInputOrSensors();
					}
				}
				leave = function() {
				}
			}
			fsm.add(_state);
		#endregion
		
		#region Run
			var _state = new State("Run");
			with (_state) {
				enter = function() {
					with (other) {
						//starting_speed = horizontal_speed;
						fnc_FSM_ResetStateAnimation();
					}
				}
				step = function() {
					with (other) {
						//part_emitter_region(Game.particle_system, particle_emitter, x-10, x+10, y-10, y, ps_shape_rectangle, ps_distr_gaussian);
						//part_emitter_region(Game.particle_system, particle_emitter, camera_get_view_x(VIEW), 2*camera_get_view_x(VIEW), camera_get_view_y(VIEW), 2*camera_get_view_y(VIEW), ps_shape_rectangle, ps_distr_gaussian);
						//part_emitter_burst(Game.particle_system, particle_emitter, part_big_star, 1000);
						
					
						//horizontal_speed = clamp(horizontal_speed + sign(horizontal_input) * horizontal_acceleration - sign(horizontal_input) * (horizontal_input == 0) * horizontal_deceleration, -max_horizontal_speed, max_horizontal_speed);
						if (fnc_OnGround()) {
							curr_max_jumps = 0;
							horizontal_speed = lerp(horizontal_speed, max_horizontal_speed * sign(horizontal_input), horizontal_acceleration);
							//horizontal_speed = lerp(starting_speed, max_horizontal_speed * sign(horizontal_input), horizontal_acceleration);
						}
						else {
							horizontal_speed = lerp(horizontal_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
							//horizontal_speed = lerp(starting_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
						}
						fnc_ProcessHorizontalMove();
		
						//vertical_speed = clamp(vertical_speed + sign(vertical_input) * vertical_acceleration - sign(vertical_input) * (vertical_input == 0) * vertical_deceleration, -max_vertical_speed, max_vertical_speed);
						vertical_speed = clamp(vertical_speed + gravity_acceleration, -jump_speed, max_vertical_speed);
						fnc_ProcessVerticalMove();
			
						fnc_FSM_Animate();
						fnc_GetInputOrSensors();
					}
				}
				leave = function() {
				}
			}
			fsm.add(_state);
		#endregion
		
		#region Jump
			var _state = new State("Jump");
			with (_state) {
				enter = function() {
					with (other) {
						if (Game.option_value[? "Sounds"]) {
							jump_sound = audio_play_sound(snd_Jump, 1, false);		
						}
						//starting_speed = horizontal_speed;
						fnc_FSM_ResetStateAnimation();					
					}
				}
				step = function() {
					with (other) {
						part_particles_create(Game.particle_system, x, y, part_small_star, 20*(curr_max_jumps+1));
						if (curr_max_jumps < max_jumps && vertical_input != 0) {
							vertical_speed = -jump_speed;
							curr_max_jumps++;
						}
					
						horizontal_speed = lerp(horizontal_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
						//horizontal_speed = lerp(starting_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
					
						fnc_ProcessHorizontalMove();
						vertical_speed = clamp(vertical_speed + gravity_acceleration, -jump_speed, max_vertical_speed);
						fnc_ProcessVerticalMove();
					
						fnc_FSM_Animate();
						fnc_GetInputOrSensors();
					}
				}
				leave = function() {
				}
			}
			fsm.add(_state);
		#endregion
		
		#region SuperJump
			var _state = new State("SuperJump");
			with (_state) {
				enter = function() {
					with (other) {						
						//starting_speed = horizontal_speed;
						if (alarm[2] <= 0 && superjump_input != 0) {							
							if (Game.option_value[? "Sounds"]) {
								doublejump_sound = audio_play_sound(snd_DoubleJump, 1, false);		
							}
							vertical_speed = -2*jump_speed;
							alarm[2] = superjump_cooldown;
						}
						fnc_FSM_ResetStateAnimation();					
					}
				}
				step = function() {
					with (other) {		
						if (vertical_speed < 0) {
							part_particles_create(Game.particle_system, x, y, part_big_star, 50);							
						}
						horizontal_speed = lerp(horizontal_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
						//horizontal_speed = lerp(starting_speed, max_horizontal_speed * sign(horizontal_input), horizontal_air_acceleration);
					
						fnc_ProcessHorizontalMove();
						vertical_speed = clamp(vertical_speed + gravity_acceleration, -2*jump_speed, max_vertical_speed);
						fnc_ProcessVerticalMove();
					
						fnc_FSM_Animate();
						fnc_GetInputOrSensors();						
					}
				}
				leave = function() {
				}
			}
			fsm.add(_state);
		#endregion
		
		#region Die
			var _state = new State("Die");
			with (_state) {
				enter = function() {
					with (other) {
						part_particles_create(Game.particle_system, x, y, part_player_fragment, 300);	
					}
				}
				step = function() {
				}
				leave = function() {
				}
			}
			fsm.add(_state);
		#endregion
	#endregion
	
	#region Transitions
		fsm.add_transition(new Transition("Idle", "Run", function() {
			return horizontal_input != 0;
		}));
	
		fsm.add_transition(new Transition("Idle", "Jump", function() {
			return curr_max_jumps < max_jumps && vertical_input != 0;
		}));
	
		fsm.add_transition(new Transition("Jump", "Idle", function() {
			return vertical_speed >= 0;
		}));
			
		fsm.add_transition(new Transition("Run", "Idle", function() {
			return horizontal_input == 0 && vertical_input == 0;
		}));
	
		fsm.add_transition(new Transition("Run", "Jump", function() {
			return curr_max_jumps < max_jumps && vertical_input != 0;
		}));
		
		fsm.add_transition(new Transition(["Idle","Run"], "SuperJump", function() {
			return fnc_OnGround() && superjump_input != 0;
		}));
		
		fsm.add_transition(new Transition("SuperJump", "Idle", function() {
			return vertical_speed >= 0;
		}));
		
		fsm.add_transition(new Transition(["Idle","Run","Jump","SuperJump"], "Die", function() {
			return hp <= 0;
		}));
	
	#endregion
	
	fsm.init("Idle");
	
	horizontal_input = false;
	vertical_input = false;
	
	
	
#endregion

#region Lighting

	light = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_point_hd, 0, x, y);

	with (light) {
		blend = c_white;
		xscale = 0.5;
		yscale = 0.5;
		x = other.x - other.sprite_xoffset + other.sprite_width/2;
		y = other.y - other.sprite_yoffset + other.sprite_height/2;
		visible = true;
	}


#endregion


fnc_FSM_ResetStateAnimation();