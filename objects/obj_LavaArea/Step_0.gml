if (ENABLE_LIVE && live_call()) return live_result;
/*if  (keyboard_check_pressed(ord("P"))) {
	paused = !paused;
}*/
if (!paused) {
	
	step++;
	var _gain = 0.3 + clamp( -0.001116071 * distance_to_object(obj_Player) + 1.428571429, 0, 1)*0.7;
	//show_debug_message(_gain);
	audio_sound_gain(volcano_sound, _gain, 0);
	
	//var _lava_speed = (1 + step/5000);
	var _lava_speed = (1 + max(0, (1-y/room_height))*3);
	//if (step % 600 == 0) show_debug_message(_lava_speed);
	y = y - _lava_speed;
	height = min(room_height - y, Game.adjusted_resolution_height*2);
	
	// Shake
	if (alarm[1] == -1 && alarm[2] == -1) {
		alarm[1] = irandom_range(60,120);
	}
	
	// Smoke	
	var _x = irandom_range(0, room_width);
	var _y = y-64;
	if (random(1)<0.2) {
		part_particles_create(Game.particle_system, _x, _y, part_smoke, 6);
	}
	
}


with (light) {
	blend = $45f6ff;
	xscale = 10;
	yscale = 5;
	x = other.x + room_width/2;
	y = other.y
}