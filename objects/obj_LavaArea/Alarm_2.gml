/// @description End of shake pause
Game.camera_shake = true;
Game.screenshake_strength = floor(5 + (1-max(0, y/room_height))*10);
alarm[1] = irandom_range(60, 120);