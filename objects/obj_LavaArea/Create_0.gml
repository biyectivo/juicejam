y = room_height;
paused = true;
sprite_index = spr_Lava_1;
step = 0;
alarm[0] = room_speed * 8;
height = 0;
volcano_sound = noone;

light = new BulbLight(obj_BulbRenderer.renderer, spr_glr_light_mask_point, 0, x, y);


part_smoke = part_type_create();
part_type_size(part_smoke, 5, 10, 0, 0);
part_type_life(part_smoke, 60*4, 60*6);
part_type_shape(part_smoke, pt_shape_smoke);
part_type_scale(part_smoke, 0.6, 0.6);
part_type_color2(part_smoke, $666666, $999999);
part_type_blend(part_smoke, false);
part_type_alpha3(part_smoke, 0.05, 0.3, 0);
part_type_speed(part_smoke, 0.2, 0.4, 0, 0);
part_type_direction(part_smoke, 70, 110, 0, 0);
	