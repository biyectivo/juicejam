fnc_BackupDrawParams();

// Make the distortion smooth instead of pixely
gpu_set_tex_filter(true);
// Repeat the texture while the shader moves the coordinates outside (0,1)
gpu_set_texrepeat(true);

shader_set(shd_Water);
var _displacement_sampler = shader_get_sampler_index(shd_Water, "displacement_map");
var _time_uniform = shader_get_uniform(shd_Water, "time");
var _strength_uniform = shader_get_uniform(shd_Water, "strength");
texture_set_stage(_displacement_sampler, sprite_get_texture(spr_Noise,0));
shader_set_uniform_f(_time_uniform, current_time/1000);
shader_set_uniform_f(_strength_uniform, 3.5);

var _w = room_width;
var _h = height;
var _spr_w = sprite_get_width(sprite_index);
var _spr_h = sprite_get_height(sprite_index);

for (var _i=0; _i<ceil(_w/_spr_w); _i++) {
	for (var _j=0; _j<ceil(_h/_spr_h); _j++) {
		draw_sprite_ext(sprite_index, 0, x+_i*_spr_w, y+_j*_spr_h, 1, 1, image_angle, image_blend, 0.9);
	}
}

shader_reset();

fnc_RestoreDrawParams();